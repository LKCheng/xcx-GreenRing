//app.js
App({
  onLaunch: function () {
   
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null,

    //回收订单参数
    companyId: '',
    companyName: '',
    addressId: '',
    addressName: '',
    statusId: '',
    statusName: '',
    boxNumber: '',
    PETItems: '',
    PPItems: '',
    firstList: '',
    secondList: '',
    thirdList: '',
    fourthList: '',
    managerId: '',
    managerName: '',
    managerMobile: '',
    uncategorizedList: [],
    EnterpriseSuit:0,
    // 采购
    purchasingManagerId: '',
    purchasingManagerName: '',
    purchasingManagerCode: '',
    //销售
    purchasingSalesleaderId:'',
    purchasingSalesleaderName: '',
    purchasingSalesleaderCode: '',
    workerList: [],//临时工
    lastWorkerList: [],
    driverId: '',
    driverName: '',
    driverCode: '',
    carId: '',
    carNum: '',
    //申请辅材
    materielList:'',
    // boxNum: 0,
    // bagNum: 0,
    // glovesNum: 0,
    // masksNum: 0,
    warehouseName: '',
    warehouseId: '',
    SpaceBagInfo:{},//提取辅材添加的太空包信息
    auxiliaryInformation:[],//提取辅材的录入辅材信息
    //辅材出库
    glovesNumOut: 0,
    masksNumOut: 0,
    steelList:[],
    cargo_list:[],
   
    storage_net_weight:'',//记录入库交接货品净重

    orderStatus: '',//订单的状态
    productList: [],//选择的货品数据
    payName: '',//开支类型的名字
    payId: '',//开支类型的id
    payInfoList: [], //开支的list
    accountName: '',//收款银行名称
    accountId: '',//收款账户id
    transferStorageList: [],//入库交接的货物列表
    HandoverGoods:"",//交接选择更改的货品名
    useName: '',//选择用途name
    useId: '',

    payeePosition: '',//领款人
    payeeName: '',//领款人
    payeeId: '',//领款人

    reimbursementList: [],//报销信息的列表 

    Sorting_point:[],//分拣点信息
    sorting_id:'',//  仓库的id
    sorting_info:'',//已选择的分拣点信息
    sort_point:'',//已确认的分拣点id

    tabsList:[],//所有的货品信息
    
    pGoodName: '',//采购货品的
    pGoodId: '',//采购货品的
    sortGoodId: '',//分拣货品
    sortGoodName: '',//分拣货品
    goods_information:[],//货品信息
    sortList: [],
    expense_info:[],//

    //销售
    clientId: '',
    clientName: '',
    firstClassificationList:[],//一级货品分类列表
    selectedFristName:'',//记录被选中的一级分类名



    clientFactory: '',
    customerInfo:'',//选择客户
    dockingInfo: {}, //客户对接人信息

    pickList: [], //新建的时候所有选项

    getMaterialList: [],//提取辅材的列表
    materialList: [],//货品的list
    parentMaterialType: '',//辅材的上一级类型
    standardQuotation: '',//标准报价

    orderDetail: {},//订单详情
    enterGoodsName: '',
    enterGoodsId: '',//提货时被选择货品的id

    salesPayName:"",//称重开支名
    salesPayId:"",//称重开支id
    salesPayInfoLists:[],//称重开支信息
    Weighed_Goods_Information:{},//称重获取的原始货品信息
    newGoodsInfo:[],//填写的货品信息
    weighMateriel:[],//辅材的信息
    cargo_info_list_len:"",//货品的品类个数
    PerfectInformation_info:'',
     
    SinglePerson:{},//下单人 
    perfectInfo:{},
    salesPayInfoList:[],//完善信息的开支信息
    payType:'',
  },
  //下拉刷新
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading() //在标题栏中显示加载

    //模拟加载
    
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
   
  },
})


