let url = 'https://testadmin.lhdrr.com/'

module.exports = {
  query(data) {
    var back = data.back
    let sessionId = wx.getStorageSync('sessionId');
    let header = { 'content-type': 'application/json', 'Cookie': 'PHPSESSID=' + sessionId }
   
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      method: 'GET',
      url: url + data.url,
      data: { ...data.params },
      header: header,
      complete: function (res) {
        wx.hideLoading();
      },
      fail: function (res) {
        wx.hideLoading()
        wx.showToast({
          title: '请求错误',
          icon: 'error',
          mask: true,
        });
        back(false);
      },
      success: function (res) {
        wx.hideLoading()
        console.log(res.data)
        back(res.data);
      }
    })
  },
  create(data) {
    var back = data.back
    let sessionId = wx.getStorageSync('sessionId');
    let header = { 'content-type': 'application/json', 'Cookie': 'PHPSESSID=' + sessionId }
    console.log(data)
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      method: 'POST',
      url: url + data.url,
      data: { ...data.params },
      header: header,
      complete: function (res) {
        wx.hideLoading();
      },
      fail: function (res) {
        wx.hideLoading()
        wx.showToast({
          title: '请求错误',
          icon: 'error',
          mask: true,
        });
        back(false);
      },
      success: function (res) {
        wx.hideLoading()
        back(res.data);
      }
    })
  }
}