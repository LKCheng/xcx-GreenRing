const app = getApp()

module.exports = {
  clear() {

    //回收订单参数
    app.globalData.companyId = '',
      app.globalData.companyName = '',
      app.globalData.addressId = '',
      app.globalData.addressName = '',
      app.globalData.statusId = '',
      app.globalData.statusName = '',
      app.globalData.boxNumber = '',
      app.globalData.PETItems = '',
      app.globalData.PPItems = '',
      app.globalData.firstList = '',
      app.globalData.secondList = '',
      app.globalData.thirdList = '',
      app.globalData.fourthList = '',
      app.globalData.managerId = '',
      app.globalData.managerName = '',
      app.globalData.managerMobile = '',
      app.globalData.uncategorizedList = [],

      // 采购
      app.globalData.purchasingManagerId = '',
      app.globalData.purchasingManagerName = '',
      app.globalData.purchasingManagerCode = '',
      app.globalData.workerList = [],//临时工
      app.globalData.lastWorkerList = [],
      app.globalData.driverId = '',
      app.globalData.driverName = '',
      app.globalData.driverCode = '',
      app.globalData.carId = '',
      app.globalData.carNum = '',
      //申请辅材
      // app.globalData.boxNum = 0,
      // app.globalData.bagNum = 0,
      // app.globalData.glovesNum = 0,
      // app.globalData.masksNum = 0,
      app.globalData.materielList = '',

      app.globalData.warehouseName = '',
      app.globalData.warehouseId = '',
      //辅材出库
      app.globalData.glovesNumOut = 0,
      app.globalData.masksNumOut = 0,

      app.globalData.orderStatus = '',//订单的状态
      app.globalData.productList = [],//选择的货品数据
      app.globalData.payName = '',//开支类型的名字
      app.globalData.payId = '',//开支类型的id
      app.globalData.payInfoList = [], //开支的list
      app.globalData.accountName = '',//收款银行名称
      app.globalData.accountId = '',//收款账户id
      app.globalData.transferStorageList = [],//入库交接的货物列表

      app.globalData.useName = '',//选择用途name
      app.globalData.useId = '',

      app.globalData.payeePosition = '',//领款人
      app.globalData.payeeName = '',//领款人
      app.globalData.payeeId = '',//领款人

      app.globalData.reimbursementList = [],//报销信息的列表

      app.globalData.pGoodName = '',//采购货品的
      app.globalData.pGoodId = '',//采购货品的
      app.globalData.sortGoodId = '',//分拣货品
      app.globalData.sortGoodName = '',//分拣货品

      app.globalData.sortList = [],

      //销售
      app.globalData.clientId = '',
      app.globalData.clientName = '',
      app.globalData.clientFactory = '',
      app.globalData.saleAddressId = '',
      app.globalData.saleAddressName = '',
      app.globalData.dockingInfo = {} //客户对接人信息

      // app.globalData.pickList = [], //新建的时候所有选项

      app.globalData.getMaterialList = [],//提取辅材的列表
      app.globalData.materialList = [],//货品的list
      app.globalData.parentMaterialType = '',//辅材的上一级类型
      app.globalData.standardQuotation = '',//标准报价

      app.globalData.orderDetail = {},//订单详情 
      app.globalData.enterGoodsName = ''
  }

}
