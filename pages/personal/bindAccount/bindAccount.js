// pages/personal/bindAccount/bindAccount.js
import { create } from '../../../utils/service.js'
var util = require('../../../utils/service.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username:'',
    password:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  onBlur:function(e){
    const that = this
    const inputType = e.currentTarget.dataset.type
    const value = e.detail.value

    if (inputType == 'username'){
      that.setData({
        username: value
      })
    } 
    if (inputType == 'password') {
      that.setData({
        password: value
      })
    } 
  },
  onSave:function () {
   
    const that = this
    const username = that.data.username
    const password = that.data.password
    const data = { username, password}
    
    // wx.navigateTo({ url: '../personal' })
   
    // util.create('api/user/bind_admin', data)
  
    create({

      url: 'api/user/bind_admin',
      params: { ...data },
      back: function (data) {
        console.log(data)
        if (data.code === 200) {
          wx.navigateBack({
            delta: 2
          })
          console.log(111)
        }else{
          wx.showToast({
            title: "用户名或密码错误",
            icon:"none",
            duration:1000

          })
        }
       
      }
    })
  }
})