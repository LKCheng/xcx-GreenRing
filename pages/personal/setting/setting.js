// pages/personal/setting/setting.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    actionSheetHidden: true,
    actionSheetItems: ['item1', 'item2', 'item3']
  },

  open: function () {
    wx.showActionSheet({
      itemList: ['退出登录后将无法及时接收到通知', '退出登录'],
      success: function (res) {
        if (!res.cancel) {
          console.log(res.tapIndex)
        }
      }
    });
  },
  clickme: function () {
    wx.navigateTo({
      url: '../bindAccount/bindAccount',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  onsignOut:function(){
    const admin = wx.getStorageSync('admin_info')
    console.log(admin)
    wx.removeStorageSync('admin_info')
   
    console.log(admin)
    wx.navigateTo({
      url: '../bindAccount/bindAccount',
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  listenerButton: function() {

    this.setData({

      //取反

        actionSheetHidden: !this.data.actionSheetHidden

    });

},



listenerActionSheet:function() {

  this.setData({

    actionSheetHidden: !this.data.actionSheetHidden

  })

},
})