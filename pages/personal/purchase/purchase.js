var cityData = require('../../../utils/city.js');
// var util = require('../../../utils/service.js');
import { create, query } from '../../../utils/service.js'


const app = getApp()
// page/one/index.js
Page({
  data: {
    list: [],
    content: [],
    orderList: [],
    px: [],
    qyopen: false,
    nzopen: false,
    pxopen: false,
    nzshow: true,
    pxshow: true,
    qyshow: true,
    isfull: false,
    cityleft: cityData.getCity(),
    citycenter: {},
    cityright: {},
    province: '',
    city: '',
    district: '',
    shownavindex: '',
    Searched:false,//是否被搜索过
    order: '',
    current: '',//当前页码
    showTip: false,
    tip: '加载中...',
    branchList: [],
    districtId: '',//查询的城市id
    orderId: '',//查询的订单id
    empty: false,
    operate_add:'',
    states:''
  },
  onShow: function () {
    const that = this
    const states = that.data.orderId
    query({
      url: 'api/waste/purchase/index',
      params: {states},
      back: function (data) {
        console.log(data)
        if ( data.code  &&  data.code.code === 400){
          that.setData({
            empty:true
          })
          return;
        }
        const operate_add = data.operate_add
        const list = data.rows
        const orderList = data.state_search
        const newList = [{ value: '全部订单', key: '' }]
        orderList && orderList.map(item => {
          const orderItem = {}
          orderItem.value = item.text
          orderItem.key = item.value
          newList.push(orderItem)
        })
        that.setData({
          orderList: newList,
          list: list,
          current: 1,
          empty: false,
          operate_add: operate_add
        })
      }
    })
    
    let branch_text = wx.getStorageSync('branch_text');
    let branch_id = wx.getStorageSync('branch_id');
    if (branch_text){
      const branchList = [{ value: branch_text, key: branch_id }]
      that.setData({
        branchList: branchList
      })
    }else{
      //获取选项
      query({
        url: 'api/ajax/sys_config',
        params: {},
        back: function (data) {
          const pickList = data.result
          app.globalData.pickList = pickList
          const branchList = pickList.branch
          // const newList = [{ value: '全部区域', key: '' }]
          //   newList.push(branchList)
          //   console.log("newList")
          //   console.log(newList)
          that.setData({
            branchList: branchList
          })
        }
      })
    }

 


   
  },
  listqy: function (e) {

    var that = this
    if (that.data.nzopen) {
      that.setData({
        nzopen: false,
        pxopen: false,
        qyopen: false,
        nzshow: false,
        pxshow: true,
        qyshow: true,
        isfull: false,
        shownavindex: 0
      })
      that.hidebg()
    } else {
      that.setData({
        content: that.data.branchList,
        nzopen: true,
        pxopen: false,
        qyopen: false,
        nzshow: false,
        pxshow: true,
        qyshow: true,
        isfull: true,
        shownavindex: e.currentTarget.dataset.nav
      })
    }

  },
  list: function (e) {
    var that = this
    if (that.data.nzopen) {
      that.setData({
        nzopen: false,
        pxopen: false,
        qyopen: false,
        nzshow: false,
        pxshow: true,
        qyshow: true,
        isfull: false,
        shownavindex: 0
      })
      that.hidebg()
    } else {
      that.setData({
        content: that.data.orderList,
        nzopen: true,
        pxopen: false,
        qyopen: false,
        nzshow: false,
        pxshow: true,
        qyshow: true,
        isfull: true,
        shownavindex: e.currentTarget.dataset.nav
      })
    }
  },
  hidebg: function (e) {

    this.setData({
      qyopen: false,
      nzopen: false,
      pxopen: false,
      nzshow: true,
      pxshow: true,
      qyshow: true,
      isfull: false,
      shownavindex: 0
    })
  },
  onSelectCity: function (e) {
    // 选择的省市区
    const that = this

    const shownavindex = that.data.shownavindex

    if (shownavindex == 1) {

      const order = e.target.dataset.order
      const orderId = e.target.dataset.id

      that.setData({
        order: order,
        orderId: orderId
      })
    }

    if (shownavindex == 2) {
      const district = e.target.dataset.order
      const districtId = e.target.dataset.id
      that.setData({
        district: district,
        districtId: districtId
      })
    }
    that.hidebg()
  },
  addNew: function (e) {
    wx.navigateTo({
      url: "./recyclingOrder/recyclingOrder"
    })
  },
  onLook: function (e) {
    const id = e.currentTarget.dataset.id
    const mode = e.currentTarget.dataset.mode
    app.globalData.orderStatus = mode

    wx.navigateTo({
      url: "./orderDetail/orderDetail?id=" + id
    })
  },
  onReachBottom() {
    const that = this
    const current = that.data.current
    const oldList = that.data.list
    const Searched = that.data.Searched
    if (Searched === false){
      wx.showNavigationBarLoading() //在标题栏中显示加载
      that.setData({
        showTip: true
      })
      query({
        url: 'api/waste/purchase/index',
        params: { page: current + 1 },
        back: function (data) {
          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
          const code = data.code
          if (code === 200) {
            let list = data.rows
            list && list.map(item => {
              oldList.push(item)
            })
            if (data.msg == '没有更多数据了。'){
              wx.showToast({
                title: data.msg,
                icon: 'none',
                duration: 1000
              })
            }
           
            that.setData({
              // orderList: ['全部订单', '订单1', '订单1', '订单1', '订单1', '订单1', '订单1'],
              list: oldList,
              current: current + 1
            })
          }
          if (code === 400) {
            const tip = data.msg
            wx.showToast({
              title: data.msg,
              icon: 'none',
              duration: 2000
            });
          }

        }
      })
    }
    

  },
  onSearch: function () {
    const that = this
    const states = that.data.orderId
    const branch_id = that.data.districtId
    console.log(states, branch_id)
    
    query({
      url: 'api/waste/purchase/index',
      params: { states, branch_id },
      back: function (data) {
        let list = data.rows
        const orderList = data.state_search
        const newList = []
        orderList && orderList.map(item => {
          const orderItem = {}
          orderItem.value = item.text
          orderItem.key = item.value
          newList.push(orderItem)
        })
        if (data.code === 400 || !data.rows) {
          list = [] 
          that.setData({
            // orderList: newList,
            list: list,
            empty: true,
            current: 1
          })
          return;
        }
        that.setData({
          // orderList: newList,
          list: list,
          current: 1,
          empty: false,
          Searched:true
        })
      }
    })
  },
  onOperate: function (e) {
    const operateType = e.currentTarget.dataset.operate
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: "./orderDetail/orderDetail?id=" + id
    })
    

  }
})