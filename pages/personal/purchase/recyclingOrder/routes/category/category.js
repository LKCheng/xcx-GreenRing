import { create, query } from '../../../../../../utils/service.js'

const app = getApp()
var num = 0
Page({
  data: {
    tabs: [],
    activeIndex: 0,

    firstList: '',
    secondList: '',
    thirdList: '',
    fourthList: '',
    status: '',
    list: [],
    valuesList: []
  },
  onLoad: function (options) {

    console.log(options)
    const id = options.id
    console.log("id")
    console.log(id)
    var that = this
    Array.prototype.del = function (n) {　//n表示第几项，从0开始算起。
      //prototype为对象原型，注意这里为对象增加自定义方法的方法。
      if (n < 0)　//如果n<0，则不进行任何操作。
        return this;
      else
        return this.slice(0, n).concat(this.slice(n + 1, this.length));
      /*
       concat方法：返回一个新数组，这个新数组是由两个或更多数组组合而成的。
       这里就是返回this.slice(0,n)/this.slice(n+1,this.length)
       组成的新数组，这中间，刚好少了第n项。
       slice方法： 返回一个数组的一段，两个参数，分别指定开始和结束的位置。
      */
    }
    if (id == 1) {

      query({
        url: 'api/ajax/waste_cate_tree',
        params: {},
        back: function (data) {
          let list = data.result
          // list = list.del(0)
          console.log(list)
          const tabs = []
          list && list.map(item => {
            tabs.push(item.name)
          })
          let index
          for (let i = 0; i < list.length; i++) {
            index = i
          }


          const firstList = list[0]
          const secondList = list[1]
          const thirdList = list[2]
          const fourthList = list[3]

          firstList.child && firstList.child && firstList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          secondList.child && secondList.child && secondList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          thirdList.child && thirdList.child && thirdList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          fourthList.child && fourthList.child && fourthList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          console.log(secondList.child)
          const lastFirstList = app.globalData.firstList
          const lastSecondList = app.globalData.secondList
          const lastThirdList = app.globalData.thirdList
          const lastFourthList = app.globalData.fourthList

          console.log("lastSecondList")
          console.log(typeof (lastSecondList))
          console.log(lastSecondList)

          that.setData({
            list: list,
            tabs: tabs,
            index: index,
            firstList: typeof (lastFirstList) == 'object' ? lastFirstList : firstList,
            secondList: typeof (lastSecondList) == 'object' ? lastSecondList : secondList,
            thirdList: typeof (lastThirdList) == 'object' ? lastThirdList : thirdList,
            fourthList: typeof (lastFourthList) == 'object' ? lastFourthList : fourthList,
            status: true
          })
        }
      })
    }

    if (id == 2) {
      query({
        url: 'api/ajax/waste_cate_tree',
        params: {},
        back: function (data) {
          const list = data.result
          console.log(list)
          list && list.map(item => {
            item.value = item.id
          })

          const uncategorizedList = app.globalData.uncategorizedList

          that.setData({
            list: uncategorizedList.length > 0 ? uncategorizedList : list,
            status: false
          })
        }
      })
    }
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });

  },
  /**
   * 树数组最后一个节点
   * @param {array} 数据
   * @param {number} id
   */
  handleArray: function (DATA, ID) {
    DATA.filter((item) => {
      if (item.id == ID[num]) {
        if (item.child) {
          num++
          this.handleArray(item.child, ID)
          return
        } else {
          item.checked = !item.checked
        }
      }
    })
  },

  onChange: function (e) {
    const { list } = this.data
    // 选择分类的id
    const cur = e.currentTarget.dataset
    const pid = cur.pid
    const zid = cur.zid
    const cid = cur.cid
    num = 0
    this.handleArray(list, [pid, zid, cid])
    this.setData({
      list: list
    });
  },
  WorkCheckboxChange: function (e) {
    var list = this.data.list, values = e.detail.value;
    console.log(values)
    console.log(list)
    for (var i = 0, lenI = list.length; i < lenI; ++i) {
      list[i].checked = false;

      for (var j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (list[i].value == values[j]) {
          list[i].checked = true;
          break;
        }
      }
    }

    this.setData({
      list: list
    });
  },
  onOk: function (e) {
    var that = this
    const firstList = that.data.firstList
    const secondList = that.data.secondList
    const thirdList = that.data.thirdList
    const fourthList = that.data.fourthList
    const uncategorizedList = that.data.list

    app.globalData.firstList = firstList
    app.globalData.secondList = secondList
    app.globalData.thirdList = thirdList
    app.globalData.fourthList = fourthList

    console.log("secondList")
    console.log(secondList)

    app.globalData.uncategorizedList = uncategorizedList


    // wx.navigateTo({ url: '/pages/personal/purchase/recyclingOrder/recyclingOrder' }) 

    wx.navigateBack({
      delta: 1
    })

  },
  onLook: function () {

  }
});