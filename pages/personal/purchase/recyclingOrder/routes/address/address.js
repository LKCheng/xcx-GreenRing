// pages/personal/purchase/recyclingOrder/routes/address/address.js
import { create, query } from '../../../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [{ address: "龙华区华荣路484号工厂", name: "刘老大", phone: "13482719301", position: "仓管", id: 1 }, { address: "坪山区深汕路深圳市大工业区体育中心东南623号", id: 2, name: "吴洁", phone: "15673829102", position: "保安" }],
    selectedId: '',
    addressName: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this

    const customer_id = options.id

    query({
      url: 'api/ajax/factory_list',
      params: { customer_id },
      back: function (data) {
        const list = data.result
        that.setData({
          list: list
        })
      }
    })

    const addressId = app.globalData.addressId
    that.setData({
      selectedId: addressId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSelect: function (e) {//选择地址
    var that = this
    const data = that.data
    const id = e.currentTarget.dataset.addressid
    const name = e.currentTarget.dataset.addressname

    const selectedId = that.data.selectedId
    if (id == selectedId) {
      that.setData({
        selectedId: ''
      })
      return;
    }
    that.setData({
      selectedId: id
    })

    app.globalData.addressId = id
    app.globalData.addressName = name

    // wx.navigateTo({ url: '/pages/personal/purchase/recyclingOrder/recyclingOrder' })
    wx.navigateBack({
      delta:1
    })
  }
})