// pages/personal/purchase/orderDetail/enterProductInfo/enterProductInfo.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    id:'',
    status: '',
    standardQuotation: '',
    total_weight_value:0,
    rough_weight_value:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const standardQuotation = app.globalData.standardQuotation

    const status = options.status
    that.setData({
      status: status,
      standardQuotation: standardQuotation
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this
    const standardQuotation = app.globalData.standardQuotation  
    const enterGoodsName = app.globalData.enterGoodsName  
    const enterGoodsId = app.globalData.enterGoodsId
    console.log("enterGoodsName")
    console.log(enterGoodsName)

    that.setData({
      name: enterGoodsName,
      standardQuotation: standardQuotation,
      id: enterGoodsId
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onCargo:function(e){
    const name = e.detail.value
    this.setData({
      name : name 
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  total_weight:function(e){
    const that = this 
    
    const total_weight_value = e.detail.value
    that.setData({
      total_weight_value: total_weight_value
    })
  },
  rough_weight:function(e){
    const that = this 
    const rough_weight_value = e.detail.value
    that.setData({
      rough_weight_value: rough_weight_value
    })
  },
  formSubmit: function (e) {
    const that = this

    const materialList = app.globalData.materialList
    const parentMaterialType = app.globalData.parentMaterialType 
    const list = app.globalData.productList
    //货品名 name
    const cargo_name = that.data.name
    const productInfo = e.detail.value
    const id = that.data.id
 
    productInfo.cargo_name = cargo_name
    productInfo.cate_id = id
    productInfo.parentMaterialType = parentMaterialType

    if (productInfo.net_weight == '' || productInfo.rough_weight == '' || productInfo.total_weight == '' || productInfo.unit_price == '') {
      wx.showToast({
        title: '请填写完整',
        icon: 'none',
        duration: 2000
      })
      return;
    }

    if (productInfo.net_weight * 1 > productInfo.total_weight * 1 || productInfo.rough_weight * 1 > productInfo.total_weight * 1) {
      wx.showToast({
        title: '请正确填写',
        icon: 'none',
        duration: 2000
      })
      return;
    }

    productInfo.total_weight = Number(productInfo.total_weight)
    productInfo.net_weight = Number(productInfo.net_weight)
    productInfo.rough_weight = Number(productInfo.rough_weight)
    productInfo.unit_price = Number(productInfo.unit_price)
    list.push(productInfo)
    app.globalData.productList = list

    app.globalData.enterGoodsName = ''
    app.globalData.enterGoodsId = ''
    console.log(list)

    // wx.navigateTo({
    //   url: '../siteDelivery/siteDelivery'
    // })

    wx.navigateBack({
      delta: 1,
    })
  },
  onPick: function () {
    wx.navigateTo({
     url: "../pickGoods/pickGoods?status=" + this.data.status,
    })
  }
})