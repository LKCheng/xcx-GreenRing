// pages/personal/purchase/orderDetail/productInfo/productInfo.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    name: '',
    weight: '',
    lossWeight: '',
    storage_net_weight:'',
    info:{},
    fatherindex:"",
    allweight:0,
    piweight:0,
    array:["直接入库","待分拣"],
    HandoverGoods:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      const that = this
      const id = options.id
      console.log(options)
      const index = options.index
      this.setData({
        fatherindex:index,
        id:id
      })     
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const list = app.globalData.cargo_list 
    console.log(list)
    this.setData({
      list:list
    })
    const index = this.index
    for(let i=0;i<list.length;i++){
      if(i == index ){
        const info = list[i]
        this.setData({
          info: info
        })
      }
    }
    //变更货品名

    const HandoverGoods = app.globalData.HandoverGoods
    this.setData({
      HandoverGoods: HandoverGoods 
    })
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },
  onChoiceGoods:function(){
    wx.navigateTo({
      url: './pickGoods/pickGoods',
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },
  allweight:function(e){
    const that = this 
    console.log(e)
    const allweight = e.detail.value
    that.setData({
      allweight: allweight
    })
  },
  piweight: function (e) {
    const that = this
    console.log(e)
    const piweight = e.detail.value
    that.setData({
      piweight: piweight
    })
  },
  listenerPickerSelected: function (e) {
    //改变index值，通过setData()方法重绘界面
    const array = this.data.array
    console.log(e.detail.value)
    const index = e.detail.value
    let payType
    for (let i = 0; i < array.length; i++) {
      if (i == index) {
        payType = array[i]
      }
    }
    app.globalData.payType = payType
    this.setData({
      index: e.detail.value,
      payType: payType
    });
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  formSubmit: function (e) {
    const that = this
    let id = that.data.id
    let storage_net_weight = e.detail.value.storage_net_weight
    const transferStorageList = app.globalData.transferStorageList
    let lossWeight = ''
    let newLossWeight = ''
    transferStorageList.map(item=>{
      if(item.id == id){
        
        if (storage_net_weight == ''){
          item.storage_net_weight = ''
        }else{
          item.storage_net_weight = storage_net_weight
          
        }
       console.log(item.storage_net_weight)
        lossWeight = (storage_net_weight - item.net_weight).toFixed(2)
        if(lossWeight>0){
          lossWeight = "+" + lossWeight
          item.lossWeight = lossWeight
          item.status = true
        }
        if(lossWeight == 0 ){
          item.lossWeight = lossWeight
          item.status = true
          
        }
        if(lossWeight < 0 ){
          // newLossWeight = lossWeight.replace(/-/,"+")
          item.lossWeight = lossWeight
          item.status = false
          
        }
      }
    })
    app.globalData.storage_net_weight = storage_net_weight
    app.globalData.transferStorageList = app.globalData.transferStorageList
  
    wx.navigateBack({
      delta: 1
    })    
  }
})