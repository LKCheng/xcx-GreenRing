// pages/personal/purchase/orderDetail/addPay/addPay.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    payName: '',
    payId: '',
    status: '',
    array:["客户承担","绿环承担"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const status = options.status
    if(status == 'new'){
      app.globalData.payName = ''
      app.globalData.payId = ''
      that.setData({
        payName: '',
        payId: '',
        status: status
      })
      return;
    }
    
    const payName = app.globalData.payName
    const payId = app.globalData.payId
    that.setData({
      payName: payName,
      payId: payId
    })
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
    const that = this
    // const status = options.status
    // if (status == 'new') {
    //   that.setData({
    //     payName: '',
    //     payId: '',
    //     status: status
    //   })
    //   return;
    // }
    const payName = app.globalData.payName
    const payId = app.globalData.payId
    that.setData({
      payName: payName,
      payId: payId
    })
  
  },
  
  listenerPickerSelected: function (e) {
    //改变index值，通过setData()方法重绘界面
    const array = this.data.array
    const index = e.detail.value
    let payType
    for (let i = 0; i < array.length; i++) {
      if (i == index) {
        payType = array[i]
      }
    }
    app.globalData.payType = payType
    this.setData({
      index: e.detail.value,
      payType: payType
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }, 
  onName: function () {
    console.log(1);
    
    const that = this
    let status  = that.data.status
    console.log(2);
    
    if(status == 'new'){
      wx.navigateTo({
        url: '../pickPayType/pickPayType?status=' + status,
        fail: function(error) {
          console.log(1222);
          console.log(error);
          
        }
      })
  
    console.log(3);
    }else{
    console.log(4);
      wx.navigateTo({
        url: '../pickPayType/pickPayType'
      })
    }
    
  },
  formSubmit: function (e) {
    const that = this
    const list = app.globalData.payInfoList
    const payInfoList = e.detail.value
    // payInfoList.usage = that.data.payName

    function publicNum(obj) {
      return -obj
    }

    const payType = that.data.payType
    if (payType == "绿环承担") {
      payInfoList.price = publicNum(payInfoList.price)
    }

    if (payInfoList.price == '' || payInfoList.usage == '' || !payType) {
      wx.showToast({
        title: '请填写完整',
        icon: 'none',
        duration: 1000
      })
      return;
    }

    // payInfoList.id = that.data.payId
    list.push(payInfoList)
    app.globalData.payInfoList = list

    wx.navigateBack({
      delta: 1
    })

    
  }
})