// pages/personal/purchase/orderDetail/purchasingManager/purchasingManager.js
import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    selectedId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    const EnterpriseSuit = app.globalData.EnterpriseSuit
    console.log(EnterpriseSuit)
    if (EnterpriseSuit === 1){
      query({
        url: 'api/ajax/purchase_incharge_list',
        params: {},
        back: function (data) {
          const list = data.result
          that.setData({
            list: list
          })
        }
      })
    } 
    if (EnterpriseSuit !== 1){
      query({
        url: 'api/ajax/company_service_list',
        params: {},
        back: function (data) {
          const list = data.result
          that.setData({
            list: list
          })
        }
      })
    }

   

    const purchasingManagerId = app.globalData.purchasingManagerId

    that.setData({
      // list: list,
      selectedId: purchasingManagerId
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSelect: function (e) {//选择地址
    var that = this
    const data = that.data
    const id = e.currentTarget.dataset.id
    const name = e.currentTarget.dataset.name
    const code = e.currentTarget.dataset.code

    const selectedId = that.data.selectedId
    if (id == selectedId) {
      that.setData({
        selectedId: ''
      })
      return;
    }
    that.setData({
      selectedId: id
    })

    app.globalData.purchasingManagerId = id
    app.globalData.purchasingManagerName = name
    app.globalData.purchasingManagerCode = code

    // wx.navigateTo({ url: '/pages/personal/purchase/orderDetail/assignOrder/assignOrder' })

    wx.navigateBack({
      delta:1
    })
  }
})