// pages/personal/purchase/orderDetail/transferSuccess/transferSuccess.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:''
  },
  onlook:function(e){
    const that = this
   // console.log(that.options.params.id)
    wx.navigateTo({
      url: '../successOrderView/successOrderView?id=' + that.options.id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    console.log(options)
    const id = options.id
    that.setData({
      id:id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (e) {
    //console.log(e)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  onBack: function () {
    const that = this
    const page = getCurrentPages()
    wx.navigateBack({
      delta: page.length - 2
    })
  }
})