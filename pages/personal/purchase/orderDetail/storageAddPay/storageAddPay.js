// pages/personal/purchase/orderDetail/addPay/addPay.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    payName: '',
    payId: '',
    status: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const status = options.status
    console.log(status)
    
 
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  
  },
  
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }, 
  onName: function () {
    // console.log(1);
    
    
  },
  formSubmit: function (e) {
    const that = this
    const list = app.globalData.payInfoList
    const payInfoList = e.detail.value
    console.log(payInfoList)
    console.log(typeof payInfoList.price)
    function isNumber(obj){
      return obj === +obj
    }
    if (payInfoList.price == '' || payInfoList.expType == '') {
      wx.showToast({
        title: '请填写完整',
        icon: 'none',
        duration: 1000
      })
    } else if (isNumber(Number(payInfoList.price)) == false){
      wx.showToast({
        title: '请正确输入',
        icon: 'none',
        duration: 1000
      })
    }else{
      list.push(payInfoList)
      app.globalData.payInfoList = list
      console.log(list)
      wx.navigateBack({
        delta: 1
      })
    }

    // payInfoList.id = that.data.payId
   

    // wx.navigateTo({
    //   url: '../siteDelivery/siteDelivery'
    // })
  }
})