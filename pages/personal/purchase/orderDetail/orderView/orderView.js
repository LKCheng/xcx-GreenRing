// pages/personal/purchase/orderDetail/orderDetail.js
import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的
   */
  data: {
    id: '',//订单的id
    item: '',
    typeList: [],
    floater_info: [],//临时工列表
    orderStatus: '',// apply申请 extract提取 receive接单 wait_allot分配订单 sign签收辅材 abstract现场提货 pay废料付费 eva客户评价 handover入库交接 reimbursement采购报销 sorting入库分拣 profit预售利润
    hide: false,
    firstStep: '',
    secondStep: '',
    thirdStep: '',
    roleList: '',
    nameList: '',
    applyList: '',
    payInfoList: [],
    payInfoListClose: [],
    open: false,
    totalMoney: '',
    photoLists: [],
    return_fee_info: [],//确认交接的字段
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const id = options.id //id查询订单明细
    that.setData({
      id: id
    })
    console.log(id)
    query({
      url: 'api/waste/purchase/read',
      params: { id },
      back: function (data) {
        const item = data.result

        console.log(item)
        let hide
        if (item.state === "finish" || !item.operates) {
          hide = true
        }


        const orderStatus = item.state
        const photoList = item.cargo_images_domain
        console.log(photoList)
        const photoLists = []
        photoLists.photoList = photoList
        console.log(photoLists)
        let totalMoney = ''
        if (item.pick_cargo_data) {
          totalMoney = (item.pick_cargo_data.cargo_price - item.pick_cargo_data.purchase_expense).toLocaleString()
        }
        //时间轴
        const obj = item.timeline
        let secondStep, firstStep, thirdStep
        for (let i = 0; i < obj.length; i++) {

          if (obj[i].remark === item.state) {
            firstStep = obj[i - 1]
            secondStep = obj[i]
            thirdStep = obj[i + 1]
          }
        }
        console.log(secondStep, firstStep)


        that.setData({
          orderList: ['全部订单', '订单1', '订单1', '订单1', '订单1', '订单1', '订单1'],
          item: item,
          hide: hide,
          firstStep: firstStep,
          secondStep: secondStep,
          thirdStep: thirdStep,
          current: 1,
          orderStatus: orderStatus,
          photoLists: photoLists,
          id: id,
          totalMoney: totalMoney
        })
      }
    })


    const typeList = [{ name: "钛白不带胶PET" }, { name: "钛白不带胶PET（厚）" }, { name: "钛白不带胶PET（厚）" }, { name: "钛白不带胶PET（厚）" }, { name: "钛白不带胶PET（厚）" }, { name: "钛白不带胶PET（薄）" }]

    //步骤
    // const firstStep = "分配订单"
    // const secondStep = "采购接单"
    // const thirdStep = "申请辅材"


    const roleList = ["客户", "客服经理", "采购"]

    const nameList = ["分类箱", "太空包"]
    const applyList = ["3", "5"]

    const payInfoList = [{ id: 1, name: '透明带胶PET卷筒（带膜）', price: '0.2', type: '塑料/PET', weight: "365.1" }, { id: 2, name: '透明带胶PET卷筒（带膜）', price: '0.2', type: '塑料/PET', weight: "365.1" }, { id: 3, name: '透明带胶PET卷筒（带膜）', price: '0.2', type: '塑料/PET', weight: "365.1" }]
    const payInfoListClose = []
    if (payInfoList.length > 0) {
      payInfoListClose.push(payInfoList[0])
    }

    var totalMoney = (874032.97).toLocaleString()//合计的价钱

    that.setData({
      // item: item,
      // typeList: typeList,
      // firstStep: firstStep,
      // secondStep: secondStep,
      // thirdStep: thirdStep,
      roleList: roleList,
      // orderStatus: orderStatus,
      //提取辅材
      nameList: nameList,
      applyList: applyList,
      payInfoList: payInfoList,
      payInfoListClose: payInfoListClose,
      // totalMoney: totalMoney
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (e) {
    const that = this
    const photoList = that.photoList
    const id = that.options.id //id查询订单明细
    console.log(that.options.id)
    //const item = app.globalData.orderDetail
    query({
      url: 'api/waste/purchase/read',
      params: { id },
      back: function (data) {
        console.log(data)
        const item = data.result
        // const pick_cargo_data = item.pick_cargo_data
        console.log(item)
        //堆放方式
        const cargo_img = item.cargo_images_domain

        //现场图片
        if (item.pick_cargo_data && item.pick_cargo_data.cargo_pick_images_domain) {
          const sence_img = item.pick_cargo_data.cargo_pick_images_domain
          that.setData({
            sence_img: sence_img
          })
        }

        that.setData({
          cargo_img: cargo_img,
        })
        //订单分配信息
        if (item.allot_data) {
          let floater_info = []
          floater_info = (item.allot_data.floater_info)

          that.setData({
            floater_info: floater_info
          })
        }

        if (item.pick_cargo_data) {
          if (item.pick_cargo_data.expense_info) {
            const return_fee_info = item.pick_cargo_data.expense_info
            that.setData({
              return_fee_info: return_fee_info
            })
          }
        }

      }
    })



  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  clickimg: function (e) {
    var index = e.currentTarget.dataset.index

    const cargo_img = this.data.cargo_img
    this.previewImage(index, cargo_img)
  },
  clickSceneImg: function (e) {
    var index = e.currentTarget.dataset.index
    const sence_img = this.data.sence_img
    this.previewImage(index, sence_img)
  },
  previewImage: function (index, imgArr) {
    // console.log(e.currentTarget.dataset.index);

    // var imgArr = this.data.cargo_img;
    console.log(index)
    wx.previewImage({
      current: imgArr[index],     //当前图片地址
      urls: imgArr,               //所有要预览的图片的地址集合 数组形式
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  onAssign: function (e) {
    wx.navigateTo({
      url: "./assignOrder/assignOrder?id=" + this.data.id
    })
  },
  onAccept: function (e) {
    const that = this
    const id = that.data.id
    wx.showModal({
      title: '提示',
      content: '是否确认接单',
      success: function (res) {
        if (res.confirm) {
          create({
            url: 'api/waste/purchase/receive',
            params: { id },
            back: function (data) {
              if (data.code === 200) {
                wx.navigateTo({
                  url: "./acceptSuccess/acceptSuccess?id=" + id
                })
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },
  onApply: function (e) {
    console.log(1)
    wx.navigateTo({
      url: "./applyMaterial/applyMaterial?id=" + this.data.id
    })
  },
  onExtract: function (e) {
    wx.navigateTo({
      url: "./extractMaterial/extractMaterial?id=" + this.data.id
    })
  },
  onSign: function (e) {

    const that = this
    const id = that.data.id
    wx.showModal({
      title: '提示',
      content: '是否签收辅材',
      success: function (res) {
        if (res.confirm) {
          create({
            url: 'api/waste/purchase/signin_materiel',
            params: { id },
            back: function (data) {
              if (data.code === 200) {
                wx.navigateTo({
                  url: "./signSuccess/signSuccess?id=" + id
                })
                console.log(1)
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  onAbstract: function (e) {
    wx.navigateTo({
      url: "./siteDelivery/siteDelivery?id=" + this.data.id
    })
  },
  onPay: function () {
    wx.navigateTo({
      url: "./scrapPay/scrapPay?id=" + this.data.id
    })
  },
  onEva: function () {
    wx.navigateTo({
      url: "./eva/eva?id=" + this.data.id
    })
  }
})