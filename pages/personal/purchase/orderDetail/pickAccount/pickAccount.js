// pages/personal/purchase/orderDetail/pickAccount/pickAccount.js

import { create, query } from '../../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    selectedId:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    var that = this
    const  id = that.options.id
    console.log(id)
    query({
      url: 'api/waste/purchase/read',
      params: { id },
      back: function (data) {
        let orderDetail = data.result
        console.log(data)

        console.log(orderDetail)
        const oldList = orderDetail.gathering_data
        const accountId = app.globalData.accountId
        const pickList = app.globalData.pickList
        const list = pickList.bank_images


        const keyList = Object.keys(list)
        const newList = []
        keyList && keyList.map(item => {
          newList.push(list[item])
        })

        oldList && oldList.map(item => {
          newList && newList.map(lItem => {
            if (item.deposit_bank == lItem.value) {
              item.image = lItem.image
            }
          })
        })


        console.log("oldList")
        console.log(oldList)
        app.globalData.oldList = oldList

        that.setData({
          list: oldList,
          selectedId: accountId
        })



      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (e) {
    console.log(e)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  onSelect: function (e) {//选择公司
  var that = this
  const data = that.data
  const id = e.currentTarget.dataset.companyid
  const name = e.currentTarget.dataset.companyname
  const selectedId = that.data.selectedId
  if (id == selectedId){
    that.setData({
      selectedId: ''
    })
    return;
  }
  that.setData({
    selectedId: id
  })
  console.log(id)
  app.globalData.accountId = id
  app.globalData.accountName = name

  wx.navigateBack({
    delta: 1
  })

  // wx.navigateTo({ url: '../scrapPay/scrapPay'}) 
  }
})