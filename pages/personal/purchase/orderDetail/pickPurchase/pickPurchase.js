// pages/personal/purchase/orderDetail/pickPurchase/pickPurchase.js
import { create, query } from '../../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    pGoodName: '',
    pGoodId: '',
    sortGoodId: '',
    sortGoodName: '',
    WLvalue: '',//物料值
    ZZvalue: 0,//总重 ZZvalue,MZvalue,
    MZvalue:0,//毛重
    JZvalue:0,//净重
    remark:'',//备注，
    sorting_info:[]//入库信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    query({
      url: 'api/ajax/waste_cate_tree',
      params: {},
      back: function (data) {
        let tabsList = data.result
        app.globalData.tabsList = tabsList
        that.setData({
          tabsList: tabsList
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this
    const pGoodId = app.globalData.pGoodId
    const pGoodName = app.globalData.pGoodName
     //货品id
    const sortGoodId = app.globalData.sortGoodId
    const sortGoodName = app.globalData.sortGoodName
    that.setData({
      pGoodId: pGoodId,
      pGoodName: pGoodName,
      sortGoodId: sortGoodId,
      sortGoodName: sortGoodName
    })
    console.log(sortGoodName, sortGoodId, pGoodId, pGoodName)

   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onWLInput: function (e) {
    const that = this
    const WLvalue = e.detail.value
    that.setData({
      WLvalue: WLvalue
    })
  },
  onZZInput: function (e) {
    const that = this
    const ZZvalue = e.detail.value
    that.setData({
      ZZvalue: ZZvalue
    })
  },
  onMZInput: function (e) {
    const that = this
    const MZvalue = e.detail.value
    that.setData({
      MZvalue: MZvalue
    })
  },
  onJZInput: function (e) {
    const that = this
    console.log(e)
    const JZvalue = e.detail.value
    that.setData({
      JZvalue: JZvalue
    })
  },
  onremarkInput: function (e) {
    const that = this
    const remarkvalue = e.detail.value
    that.setData({
      remarkvalue: remarkvalue
    })
  },
  onPick: function (e) {
    const pickType = e.currentTarget.dataset.type
    if (pickType == 'get') {
      wx.navigateTo({
        url: '../pickPurchaseGoods/pickPurchaseGoods',
      })
    }
    if (pickType == 'sort') {
      wx.navigateTo({
        url: '../pickPurchaseSort/pickPurchaseSort',
      })
    }
  },
  formSubmit: function (e) {
    console.log(e)
      const that = this.data
      const list = app.globalData.goods_information
      const { sorting_info,sortGoodId, sortGoodName, WLvalue, ZZvalue, MZvalue, remarkvalue } = this.data
      const JZvalue = e.detail.value.net_wight
 
      if (sortGoodId === '' || WLvalue === '' || ZZvalue === '' || MZvalue === '' || JZvalue === '') {
        wx.showToast({
          title: '请填写完整',
          icon: 'none',
          duration: 1000
        })
      } else if (Number(ZZvalue) < Number(MZvalue) || Number(ZZvalue) < Number(JZvalue) ){
        wx.showToast({
          title: '请正确填写重量',
          icon: 'none',
          duration: 1000
        })
      }else{
        const sorting_info = { remark: remarkvalue, cargo_sort: sortGoodId, materiel_number: WLvalue, total_weight: ZZvalue, rough_weight: MZvalue, net_weight: JZvalue, sortGoodId: sortGoodId, sortGoodName: sortGoodName}


        list.push(sorting_info)
        console.log(list)
        
        wx.navigateBack({
          delta: 1
        })
      }


     
      
      // app.globalData.sortList = list
      // // 清空缓存
      // app.globalData.pGoodId = ''
      // app.globalData.pGoodName = ''
       app.globalData.sortGoodId = ''
       app.globalData.sortGoodName = ''

    
    }
  })

