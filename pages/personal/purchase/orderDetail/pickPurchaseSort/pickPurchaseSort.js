// pages/personal/purchase/orderDetail/pickPurchaseSort/pickPurchaseSort.js
import { create, query } from '../../../../../utils/service.js'
const app = getApp()

Page({
  data: {
    tabs: ["塑料", "纸", "金属", "其他"],
    activeIndex: 0,
    tabsList: [],
    tabsListItem:'',//总货品的子类
    paperList:[],//纸列表
    plasticList:[],//塑料列表
    metalList:[],//金属列表
    otherList:[],//其他列表
    selectedId: '',
  },
  onShow:function(){
    const that = this
    // const tabsList = app.globalData.tabsList
    
    Array.prototype.del = function (n) {　//n表示第几项，从0开始算起。
      //prototype为对象原型，注意这里为对象增加自定义方法的方法。
      　if (n < 0)　//如果n<0，则不进行任何操作。
        return this;
      　else
        return this.slice(0, n).concat(this.slice(n + 1, this.length));
      /*
       concat方法：返回一个新数组，这个新数组是由两个或更多数组组合而成的。
       这里就是返回this.slice(0,n)/this.slice(n+1,this.length)
       组成的新数组，这中间，刚好少了第n项。
       slice方法： 返回一个数组的一段，两个参数，分别指定开始和结束的位置。
      */
    }
    //自己增加的方法
    let tabsList = app.globalData.tabsList
    // tabsList = tabsList.del(0);　//从0算起，删除第1

    console.log(tabsList)

    that.setData({
      tabsList : tabsList
    })

    for(let i = 0 ; i <tabsList.length ; i++){
      const tabsListItem = tabsList[i]
      
      that.setData({
        tabsListItem: tabsListItem
      })
    }
    const paperList = tabsList[0].child
    const plasticList = tabsList[1].child
    const metalList = tabsList[2].child
    const otherList = tabsList[3].child
    that.setData({
      paperList:paperList,
      plasticList:plasticList,
      metalList:metalList,
      otherList:otherList
    })
  },
  onLoad: function () {
    var that = this
  
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  onSelect: function (e) {
    console.log("e")
    console.log(e)
    var that = this
    const data = that.data
    const id = e.currentTarget.dataset.id
    app.globalData.sortGoodId = id
    const name = e.currentTarget.dataset.name
    const selectedId = that.data.selectedId
    if (id == selectedId) {
      that.setData({
        selectedId: ''
      })
      return;
    }
    that.setData({
      selectedId: id
    })

    
    app.globalData.sortGoodName = name
    
    wx.navigateBack({
      delta: 1
    })

  }
});