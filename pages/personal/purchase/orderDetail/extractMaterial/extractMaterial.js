// pages/personal/purchase/orderDetail/extractMaterial/extractMaterial.js
import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    getMaterialList: [],
    payInfoListClose: [],
    status: false,
    open: false,
    message: '',
    SpaceBagInfo:[],//太空包信息
    codeValue:'',
    amountValue:'',
    AuxiliaryArr:[],
    applyList:[],
    outGoing_info:[],
    viewhide:'',
    blurapplyList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this

    const id = options.id

    query({
      url: 'api/waste/purchase/read',
      params: { id },
      back: function (data) {
        let item = data.result
        console.log(item)
        const id = item.id
        const applyList = item.apply_materiel_data.apply_materiel_info
        that.setData({
          id: id,
          applyList: applyList
        })
        console.log(applyList)
      }
    })

  },

  onOpen: function () {
    const that = this
    that.setData({
      open: true
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  onblur:function(e){
    console.log(e)
  },
  onCodeBlur: function (e) {
    const that = this
    console.log(e)
    const codeValue = e.detail.value
    that.setData({
      codeValue: codeValue
    })
  },
  onAmountBlur: function (e) {
    const that = this
    console.log(e)
    const amountValue = e.detail.value
    that.setData({
      amountValue: amountValue
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    //获取选项
    query({
      url: 'api/ajax/sys_config',
      params: {},
      back: function (data) {
        const pickList = data.result
        app.globalData.pickList = pickList
      }
    })
   

    const Auxiliary = app.globalData.Auxiliary
    const applyList = that.data.applyList
    let AuxiliaryArr = that.data.AuxiliaryArr
    if(Auxiliary){
      AuxiliaryArr.push(Auxiliary)
      console.log(1)
    }

    if(applyList){
      for (let i = 0; i < applyList.length; i++) {
        if (applyList[i].type == Auxiliary.typeInfo) {
          applyList[i].pick_amount = Auxiliary.numberInfo
          applyList[i].pick_materiel_number = Auxiliary.codeInfo
          delete applyList[i].storage_amount
          delete applyList[i].storage_materiel_number
        }
      }
    }

   
    console.log(applyList)
    app.globalData.applyList = applyList
    

    that.setData({
      AuxiliaryArr: AuxiliaryArr,
      Auxiliary:Auxiliary,
      applyList:applyList
    })
    
    //清空 缓存
    app.globalData.Auxiliary = ''
    
    
    
    
    
    
    const getMaterialList = app.globalData.getMaterialList
    let SpaceBagInfo = []
    const SpaceBagInfolist = app.globalData.SpaceBagInfo
    //console.log(SpaceBagInfolist)
    SpaceBagInfo.push(SpaceBagInfolist)
    //console.log(SpaceBagInfo)
    that.setData({
      getMaterialList: getMaterialList,
      SpaceBagInfo:SpaceBagInfo,
      SpaceBagInfolis:SpaceBagInfolist
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  
  bindNumber:function(e){
    const that = this
    const applyList = that.data.applyList
    const id = e.currentTarget.dataset.id
    const numberValue = e.detail.value
    console.log(numberValue)
    for (let i = 0; i < applyList.length; i++) {
      if (id == applyList[i].id) {
        applyList[i].pick_amount = Number(numberValue)
      }
    }
    that.setData({
      applyList: applyList
    })
    console.log(applyList)
  },
  bindCode:function(e){
    const that = this
    const applyList = that.data.applyList
    const id = e.currentTarget.dataset.id
    const codeValue = e.detail.value
    for (let i = 0; i < applyList.length; i++) {
      if (id == applyList[i].id) {
        applyList[i].pick_materiel_number = codeValue
      }
    }
    that.setData({
      applyList: applyList
    })
  },
  onScan: function () {
    // 允许从相机和相册扫码
    wx.scanCode({
      success: (res) => {
        console.log(res)
        const status = res.errMsg
        if (status == 'scanCode:ok') {
          const result = res.result
          wx.navigateTo({
            url: '../materialInfo/materialInfo?result=' + result,
          })
        }
      },
      fail: (res) => {
        console.log(res)
      }
    })
  },
  onAdd: function () {
    wx.navigateTo({
      url: "../expendMeterial/expendMeterial"
    })
  },
  onChange: function (e) {
    console.log(e)
    app.globalData.Auxiliary = ''
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../materialInfo/materialInfo?id=' + id,
    })
  },
  onBlur: function (e) {
    const that = this
    const value = e.detail.value
    that.setData({
      message: value
    })
  },
  //手指触摸动作开始 记录起点X坐标 --------删除货品
  touchstart: function (e) {
    //开始触摸时 重置所有删除
    this.data.AuxiliaryArr.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })
    this.setData({
      startX: e.changedTouches[0].clientX,
      startY: e.changedTouches[0].clientY,
      AuxiliaryArr: this.data.AuxiliaryArr
    })
  },
  //滑动事件处理
  touchmove: function (e) {
    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      startX = that.data.startX,//开始X坐标
      startY = that.data.startY,//开始Y坐标
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.AuxiliaryArr.forEach(function (v, i) {
      v.isTouchMove = false
      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
    })
    //更新数据
    that.setData({
      AuxiliaryArr: that.data.AuxiliaryArr
    })
  },
  /**
   * 计算滑动角度
   * @param {Object} start 起点坐标
   * @param {Object} end 终点坐标
   */
  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  //删除事件
  del: function (e) {
    this.data.AuxiliaryArr.splice(e.currentTarget.dataset.index, 1)
    console.log("this.data.AuxiliaryArr")
    console.log(this.data.AuxiliaryArr)
    app.globalData.AuxiliaryArr = this.data.AuxiliaryArr

    this.setData({
      AuxiliaryArr: this.data.AuxiliaryArr
    })
  },
  //手指触摸动作开始 记录起点X坐标 --------删除货品
  touchstartPay: function (e) {
    //开始触摸时 重置所有删除
    this.data.AuxiliaryArr.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })
    this.setData({
      startX: e.changedTouches[0].clientX,
      startY: e.changedTouches[0].clientY,
      AuxiliaryArr: this.data.AuxiliaryArr
    })
  },
  //滑动事件处理
  touchmovePay: function (e) {
    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      startX = that.data.startX,//开始X坐标
      startY = that.data.startY,//开始Y坐标
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.AuxiliaryArr.forEach(function (v, i) {
      v.isTouchMove = false
      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
    })
    //更新数据
    that.setData({
      AuxiliaryArr: that.data.AuxiliaryArr
    })
  },
  /**
   * 计算滑动角度
   * @param {Object} start 起点坐标
   * @param {Object} end 终点坐标
   */
  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  //删除事件
  delPay: function (e) {
    let delet = this.data.AuxiliaryArr.splice(e.currentTarget.dataset.index, 1)
    let Auxiliary = app.globalData.Auxiliary

    this.setData({
      AuxiliaryArr: this.data.AuxiliaryArr,
    })

    delet.map(item => {
      delete item.isTouchMove
      if (item = Auxiliary) {
        app.globalData.Auxiliary = ''
      }
    })
  },
  onSubmit: function () {
    const that = this
    const id = that.data.id
    //验证是否非数字
    function isNumber(obj) {
      return obj === +obj
    }

    const AuxiliaryArr = that.data.AuxiliaryArr

    let applyList = that.data.applyList
   
    console.log(applyList)
    let pick_amount
    let pick_materiel_number 
    let storage_amount_isNumber
    let storage_amount_isNull
    let materiel_info = []

    applyList && applyList.map( item =>{

      delete item.storage_amount
      delete item.storage_materiel_number
      console.log(item.pick_materiel_number)
      if (item.pick_amount == null){
        item.pick_amount  = 0
      }
      if (item.pick_materiel_number == null){
        item.pick_materiel_number = ' '
      } else if (isNumber(item.pick_amount ) == false){
        storage_amount_isNumber  = false
        console.log(2222)
      } 
      console.log(item.pick_materiel_number)
    })

    console.log(applyList)
    if (applyList == null ){
      applyList = []
    }
  
   console.log(storage_amount_isNumber)
    const pick_materiel_remark = that.data.message
    const is_draft = 0
    const data = {
      id,
      pick_materiel_remark,
      materiel_info: applyList,
      is_draft
    }
    console.log(data)
   

    if (storage_amount_isNumber == false){
      wx.showToast({
        title: '请正确填写',
        icon:'none',
        duration:1000
      })
    } else if (storage_amount_isNull == false ){
      wx.showToast({
        title: '请填写完整',
        icon: 'none',
        duration: 1000
      })
    }else{
      wx.showModal({
        title: '提示',
        content: '是否确认提交',
        success: function (res) {
          if (res.confirm) {
            create({
              url: 'api/waste/purchase/pick_materiel',
              params: { ...data },
              back: function (data) {
                if (data.code === 200) {
                  console.log(111)
                    wx.navigateTo({ url: './submitSuccess/submitSuccess?id=' + id })
                }
              }
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
  },
  onLook: function () {
    wx.navigateTo({ url: '../orderView/orderView?id=' + this.data.id })
  }
})