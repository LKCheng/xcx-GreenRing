// pages/personal/purchase/orderDetail/sorting/sorting.js
import { create, query } from '../../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    cargo_weight:'',
    sort_list: [],
    startX: 0, //开始坐标
    startY: 0,
    sorting_info: [],
    sorting_info_list:[],
    goodsInfo:{},
    netWeight:"",//入库货品,
    netArr:[],
    expense_info:[],//分拣开支的数据
    expenses_info_len:'',//开支数组的长度
    Personnel_value: '',//分拣人员
    Remake_value:'',//分拣留言
    Sorting_net_weight_aggregate:'',//分拣净重合计
    Sum_price_arr:[],//开支合计数组
    Sum_price:''//开支合计最终
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    console.log(options)
    const id = options.id
    that.setData({
      id:id
    })
    let cargo_weight = '' 
    create({
      url:'api/waste/purchase/read',
      params:{id},
      back:function(data){
        console.log(data)
        let cargo_weight = data.result.storage_sort_data.cargo_weight
        that.setData({
          cargo_weight: cargo_weight
        })
      }
    })
  },
  onlook:function(){
    const that = this
    // console.log(that.options.params.id)
    wx.navigateTo({
      url: '../orderView/orderView?id=' + that.options.id
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (e) {
    const that = this
    const expense_info = app.globalData.expense_info
    const sorting_info = app.globalData.goods_information
    console.log(sorting_info)
    const expenses_info_len = expense_info.length
    const sorting_info_list = []
    const goodsInfo = {}
    let netWeight = ''
    let netArr = []
    that.setData({
      sorting_info: sorting_info,
      expenses_info_len: expenses_info_len,
      expense_info: expense_info
    })
  
    if(sorting_info.length>0){
      for (let i = 0; i < sorting_info.length; i++) {
        let sorting_info_list = sorting_info[i]
        let netWeight = sorting_info[i].net_weight
        let arr = []
        netArr.push(netWeight)
        that.setData({
          sorting_info_list: sorting_info_list,
          netArr: netArr
        })
        
      }
    }


    let Sorting_net_weight_aggregate = ''
    let Sum_of_expenses = ''
    let Sum_price_arr = []
    let Sum_price = ''
    for(var x = 0;x<netArr.length ;x++){
      Sorting_net_weight_aggregate += parseInt(netArr[x])
      Sorting_net_weight_aggregate = parseInt(Sorting_net_weight_aggregate)
    }
    for (var y = 0; y < expense_info.length ;y++){
      let sum = expense_info[y].price
      Sum_price_arr.push(sum)
    }
   
    for (let z=0;z < Sum_price_arr.length ; z++){
      Sum_price += parseInt(Sum_price_arr[z])
      Sum_price = parseInt(Sum_price)
    }
    that.setData({
      Sorting_net_weight_aggregate: Sorting_net_weight_aggregate,
      Sum_price_arr: Sum_price_arr,
      Sum_price: Sum_price
    })



  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  onPersonnel:function(e){
    const that = this
    const Personnel_value = e.detail.value
    that.setData({
      Personnel_value: Personnel_value
    })
  },
  onRemake: function (e) {
    const that = this
    const Remake_value = e.detail.value
    that.setData({
      Remake_value: Remake_value
    })
  },
  onAdd:function(){
    wx.navigateTo({
      url: '../pickPurchase/pickPurchase',
    })
  },
  onAddExp: function () {
    wx.navigateTo({
      url: '../goodsExpenses/goodsExpenses',
    })
  },
  //手指触摸动作开始 记录起点X坐标
  touchstart: function (e) {
    //开始触摸时 重置所有删除
    this.data.sorting_info.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })
    this.setData({
      startX: e.changedTouches[0].clientX,
      startY: e.changedTouches[0].clientY,
      sorting_info: this.data.sorting_info
    })
  },
  //滑动事件处理
  touchmove: function (e) {

    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      startX = that.data.startX,//开始X坐标
      startY = that.data.startY,//开始Y坐标
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.sorting_info.forEach(function (v, i) {
      v.isTouchMove = false
      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
    })
    //更新数据
    that.setData({
      sorting_info: that.data.sorting_info
    })
  },
  /**
   * 计算滑动角度
   * @param {Object} start 起点坐标
   * @param {Object} end 终点坐标
   */
  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  //删除事件
  del: function (e) {
    const that = this
    that.data.sorting_info.splice(e.currentTarget.dataset.index, 1)
    const sorting_info = that.data.sorting_info
    let Sorting_net_weight_aggregate = that.data.Sorting_net_weight_aggregate
    let netWight = []
    let newWeight = 0
    console.log(sorting_info)
    for (let i = 0; i < sorting_info.length; i++){
      netWight.push(sorting_info[i].net_weight)
    }
    for (let j = 0; j < netWight.length; j++){
      newWeight += Number(netWight[j])
    }
    
    that.setData({
      sorting_info: sorting_info,
      Sorting_net_weight_aggregate: newWeight
   
    })
    app.globalData.goods_information = that.data.sorting_info
  },
  //手指触摸动作开始 记录起点X坐标
  prytouchstart: function (e) {
    //开始触摸时 重置所有删除
    this.data.expense_info.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })
    this.setData({
      startX: e.changedTouches[0].clientX,
      startY: e.changedTouches[0].clientY,
      expense_info: this.data.expense_info
    })
  },
  //滑动事件处理
  prytouchmove: function (e) {

    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      startX = that.data.startX,//开始X坐标
      startY = that.data.startY,//开始Y坐标
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.expense_info.forEach(function (v, i) {
      v.isTouchMove = false
      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
    })
    //更新数据
    that.setData({
      expense_info: that.data.expense_info
    })
  },
  /**
   * 计算滑动角度
   * @param {Object} start 起点坐标
   * @param {Object} end 终点坐标
   */
  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  //删除事件
  prydel: function (e) {
    const that = this
    console.log(e)
    that.data.expense_info.splice(e.currentTarget.dataset.index, 1)
    const expense_info = that.data.expense_info
    let Sum_price = that.data.Sum_price
    console.log(expense_info)
    let money = []
    let  newNumber = 0
    for (let i = 0; i < expense_info.length; i++) {
      money.push(expense_info[i].price)
    }
    console.log(money)
    for (let j = 0; j < money.length; j++) {
      newNumber += Number(money[j])
    }
    Sum_price = newNumber.toFixed(2)
    
    that.setData({
      expense_info: expense_info,
      expenses_info_len: expense_info.length,
      Sum_price: Sum_price
    })
    app.globalData.expense_info = that.data.expense_info
  },
  //提交事件
  onSubmit:function(){
    const that = this 
    const { id, Personnel_value, Remake_value, sorting_info, expense_info} = that.data
    const data = {
      id:id,
      sort_man: Personnel_value,
      sort_remark: Remake_value,
      sort_info: sorting_info,
      expense_info: expense_info,
      is_draft:0
    }
    console.log(data)
    if (sorting_info.length  <=  0 ){
      wx.showToast({
        title: '请选择入库货品',
        icon:'none',
        duration:1000
      })
    } else if (Personnel_value === '' ){
      wx.showToast({
        title: '请填写分拣人员',
        icon: 'none',
        duration: 1000
      })
    }else{
      wx.showModal({
        title: '提示',
        content: '是否确认提交',
        success: function (res) {
          if (res.confirm) {
            create({
              url: 'api/waste/purchase/storage_sort',
              params: { ...data },
              back: function (data) {
                if (data.code === 200) {
                  wx.navigateTo({
                    url: "../storageSuccess/storageSuccess?id=" + id
                  })
                  console.log(id)
                }
              }
            })
          } else {
            console.log('用户点击取消')
          }
        }
      })
    }
  }
})