import { create, query } from '../../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */

  data: {
    sorting_list:[],
    sorting_point_list: [],
    id: '',
    list: [],
    sorting_info:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    //console.log(options)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  onChosen:function(e){
     console.log(e)
    const that = this 
    const sorting_info = e.currentTarget.dataset.key
    const sort_id = e.currentTarget.dataset.idd
    this.setData({
      sorting_info: sorting_info
   })
    app.globalData.sorting_info = sorting_info
    app.globalData.sort_id = sort_id
    wx.navigateBack({
      delta:2
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (e) {
    const that = this
    query({
      url: 'api/ajax/warehouse_tree',
      params: {},
      back: function (data) {
        const sorting_list = data.result
        if (sorting_list.length > 0) {
          that.setData({
            sorting_list: sorting_list
          })
        }
        //循环遍历 将匹配的id的值赋给list
        for (let i = 0; i < sorting_list.length; i++) {
          const sorting_point_list = sorting_list[i].child
          for (let j = 0; j < sorting_point_list.length;j++){
            const sorting_point_list_id = sorting_point_list[j].parent_id
            //console.log(sorting_point_list_id)
            let getAppInfo = app.globalData.sorting_id
            if (sorting_point_list_id === getAppInfo){
              const list = sorting_point_list
              that.setData({
                list : list 
              })
              console.log(list)
            }
          }
        }
      }
    })
  },
  onClicked: function (e) {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSelect: function (e) {
    var that = this


  }
})
