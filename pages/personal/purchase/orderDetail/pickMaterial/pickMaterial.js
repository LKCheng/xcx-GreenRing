// pages/personal/purchase/orderDetail/pickMaterial/pickMaterial.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    materielList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this


    const masksNum = app.globalData.masksNum

    const pickList = app.globalData.pickList
    const oldList = app.globalData.materielList
    console.log(oldList)
    const list = pickList.materiel_type
    console.log(typeof (oldList))
    if (typeof(oldList) == 'string') {
      const keyList = Object.keys(list)
      const materielList = []
      keyList && keyList.map(item => {
        materielList.push(list[item])
      })
      that.setData({
        materielList: materielList
      })
    }else{
      that.setData({
        materielList: oldList
      })
    }

  that.setData({
    list: list
  })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addBox: function (e) {
    const that = this
    const key = e.currentTarget.dataset.key
    const materielList = that.data.materielList
    materielList && materielList.map(item => {
      if (item.key == key) {
        if (!item.num) {
          item.num = 1
        } else {
          item.num++
        }
      }
    })
    that.setData({
      materielList: materielList
    })
  },
  cutBox: function (e) {
    const that = this
    const key = e.currentTarget.dataset.key
    const materielList = that.data.materielList
    materielList && materielList.map(item => {
      if (item.key == key) {
        item.num--
      }
    })
    that.setData({
      materielList: materielList
    })
  },
  onSubmit: function () {
    var that = this.data
    const materielList = that.materielList

    app.globalData.materielList = materielList

    wx.navigateBack({
      delta: 1
    })

  }
})