// pages/personal/purchase/orderDetail/pickGoods/pickGoods.js
import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({
  data: {
    tabs: ["塑料", "纸", "金属", "其他"],
    activeIndex: 0,
    selectedId: '',
    parentname:'纸',

    firstList: '',
    secondList: '',
    thirdList: '',
    fourthList: '',
    status: '',
    list: []
  },
  onLoad: function (options) {
    console.log(options)
    const status = options.status

    var that = this
    Array.prototype.del = function (n) {　//n表示第几项，从0开始算起。
      //prototype为对象原型，注意这里为对象增加自定义方法的方法。
      　if (n < 0)　//如果n<0，则不进行任何操作。
        return this;
      　else
        return this.slice(0, n).concat(this.slice(n + 1, this.length));
      /*
       concat方法：返回一个新数组，这个新数组是由两个或更多数组组合而成的。
       这里就是返回this.slice(0,n)/this.slice(n+1,this.length)
       组成的新数组，这中间，刚好少了第n项。
       slice方法： 返回一个数组的一段，两个参数，分别指定开始和结束的位置。
      */
    }
    //自己增加的方法
    var test = new Array(0, 1, 2, 3, 4, 5);
    test = test.del(3);　//从0算起，删除第4项。
    
    if (status == 1) {

      query({
        url: 'api/ajax/waste_cate_tree',
        params: {},
        back: function (data) {
          let list = data.result
         



          const tabs = []
          list && list.map(item => {
            tabs.push(item.name)
          })

          app.globalData.materialList = list

          
          that.setData({
            list: list,
            tabs: tabs,
            firstList: firstList,
            secondList: secondList,
            thirdList: thirdList,
            fourthList: fourthList,
            status: true
          })
        }
      })
    }


    if (status == 0) {
      query({
        url: 'api/ajax/waste_cate_tree',
        params: {},
        back: function (data) {
          const list = data.result
          console.log("list")
          console.log(list)
          list && list.map(item => {
            item.value = item.id
          })
          that.setData({
            list: list,
            status: false
          })
        }
      })
    }

    const firstList = app.globalData.firstList
    const secondList = app.globalData.secondList
    const thirdList = app.globalData.thirdList
    const fourthList = app.globalData.fourthList

    if (typeof (firstList) == 'string') {
      return;
    }
    that.setData({
      firstList: firstList,
      secondList: secondList,
      thirdList: thirdList,
      fourthList: fourthList
    })

  },
  tabClick: function (e) {
  console.log(e)
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id,
      parentname:e.currentTarget.dataset.parentname 
    });
  },
  onSelect: function (e) {
    var that = this

    const activeIndex = that.data.activeIndex
    const status = that.data.status
   

    const list = that.data.list


    // console.log(secondList)
    console.log(e)

    let parentName = that.data.parentname
    let firstName = e.currentTarget.dataset.firstname
    let price = e.currentTarget.dataset.price

    if (status == 1) {

      

      const parentMaterialType = parentName + "/" + firstName

      app.globalData.parentMaterialType = parentMaterialType
      app.globalData.standardQuotation = price

    }

    


    const id = e.currentTarget.dataset.idd
    const name = e.currentTarget.dataset.name
    const selectedId = that.data.selectedId

    app.globalData.enterGoodsName = name
    app.globalData.enterGoodsId = id
    console.log(id)
    // console.log("parentName")
    // console.log(parentName)

    that.setData({
      selectedId: id
    })

   
    wx.navigateBack({
      delta:1
    })


  }
});