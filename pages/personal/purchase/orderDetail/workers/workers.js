// pages/personal/purchase/orderDetail/workers/workers.js
import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    workerList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this

    query({
      url: 'api/ajax/floater_list',
      params: {},
      back: function (data) {
        const list = data.result

        const keyList = Object.keys(list)
        const newList = []
        keyList && keyList.map(item => {
          list[item].value = list[item].id
           newList.push(list[item])
        })
  
        console.log("newList")
        console.log(newList)
        const lastWorkerList = app.globalData.lastWorkerList
        that.setData({
          workerList: lastWorkerList.length > 0 ? lastWorkerList : newList,
        })
      }
    })

    // const list = [{ id: 1, value: 1, img: "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png", name: "林大胆", code: "LH8920" }, { id: 2, value: 2, img: "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png", name: "林大胆", code: "LH8920" }, { id: 3, value: 3, img: "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png", name: "林大胆", code: "  LH8920" }, { id: 4,value: 4, img: "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png", name: "林大胆", code: "LH8920" }]

    // const lastWorkerList = app.globalData.lastWorkerList
    // that.setData({
    //   workerList: lastWorkerList.length > 0 ? lastWorkerList : list,
    // })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this 
    const addWork = app.globalData.TemporaryWorker
    that.setData({
      addWork:addWork
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  WorkCheckboxChange: function (e) {
    var workerList = this.data.workerList, values = e.detail.value;
    console.log(workerList)
    for (var i = 0, lenI = workerList.length; i < lenI; ++i) {
      workerList[i].checked = false;

      for (var j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (workerList[i].value == values[j]) {
          workerList[i].checked = true;
          break;
        }
      }
    }

    this.setData({
      workerList: workerList
    });
  },
  onAdd: function (e) {
    // var that = this
    // const PETItems = that.data.PETItems
    // const PPItems = that.data.PPItems

    // app.globalData.PETItems = PETItems
    // app.globalData.PPItems = PPItems

    wx.navigateTo({ url: '/pages/personal/purchase/orderDetail/addWorker/addWorker' })

  },
  onSave: function () {
    var that = this
    const workerList = that.data.workerList
    const list = []

    workerList && workerList.map(item=>{
      if (item.checked){
        list.push(item)
      }
    })
    app.globalData.workerList = list
    app.globalData.lastWorkerList = workerList
    // wx.navigateTo({ url: '/pages/personal/purchase/orderDetail/assignOrder/assignOrder' })

    wx.navigateBack({
      delta: 1
    })
    
  }
})