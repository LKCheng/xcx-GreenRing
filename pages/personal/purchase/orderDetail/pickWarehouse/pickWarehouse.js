// pages/personal/purchase/orderDetail/pickWarehouse/pickWarehouse.js
import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    selectedId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this

    //获取选项
    query({
      url: 'api/ajax/warehouse_tree',
      params: {},
      back: function (data) {
        const list = data.result
        const warehouseId = app.globalData.warehouseId
        that.setData({
          selectedId: warehouseId,
          list: list
        })
      }
    })  

    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSelect: function (e) {//选择公司
    var that = this
    const data = that.data
    const id = e.currentTarget.dataset.companyid
    const name = e.currentTarget.dataset.companyname
    const selectedId = that.data.selectedId
    if (id == selectedId) {
      that.setData({
        selectedId: ''
      })
      return;
    }
    that.setData({
      selectedId: id
    })

    app.globalData.warehouseId = id
    app.globalData.warehouseName = name

    // wx.navigateTo({ url: '/pages/personal/purchase/orderDetail/applyMaterial/applyMaterial' })
    wx.navigateBack({
      delta: 1
    })
  }
})