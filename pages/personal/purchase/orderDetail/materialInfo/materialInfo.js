// pages/personal/purchase/orderDetail/materialInfo/materialInfo.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    // list: [],
    num: 1,
    uploadList: [],
    mType: [],
    title: '',
    typeInfo:'',
    codeInfo:"",
    numberInfo:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this

    const id = options.id
    console.log(id)
    const result = options.result
    
    if (result) {
      const pickList = app.globalData.pickList
      const mList = app.globalData.pickList.materiel_type
      const keyList = Object.keys(mList)
      const newList = []
      keyList && keyList.map(item => {
        newList.push(mList[item])
      })

      const mType = options.result.split("-");

      newList && newList.map(item => {
        if (item.remark == mType[1]) {
          that.setData({
            title: item.value,
            id: item.key,
            uploadList: [options.result],
            mType: mType[1]
          })
        }
      })
    }

    if (id) {
      const getMaterialList = app.globalData.getMaterialList
      getMaterialList && getMaterialList.map(item => {
        const mType = item.uploadList[0].split("-");
        if (item.id == id) {
          that.setData({
            id: item.id,
            title: item.type,
            uploadList: item.uploadList,
            mType: mType
          })
        }
      })
    }

  },
  ontype:function(e){
    const that = this 
    const typeInfo = e.detail.value
    that.setData({
      typeInfo:typeInfo
    })
  },
  oncode: function (e) {
    const that = this
    const codeInfo = e.detail.value
    that.setData({
      codeInfo: codeInfo
    })
  },
  onnumber: function (e) {
    const that = this
    const numberInfo = e.detail.value
    that.setData({
      numberInfo: numberInfo
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    app.globalData.Auxiliary = ''
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onScan: function () {
    // 允许从相机和相册扫码
    const that = this
    const uploadList = that.data.uploadList
    const mType = that.data.mType

    wx.scanCode({
      success: (res) => {
        const status = res.errMsg
        const newType = res.result.split("-");
        if (mType != newType[1]) {
          wx.showToast({
            title: '请添加相同类型辅材',
            icon: 'none',
            duration: 2000
          })
          return;
        }
        uploadList && uploadList.map(item => {
          if (res.result == item) {
            wx.showToast({
              title: '请勿重复添加',
              icon: 'none',
              duration: 2000
            })
          } else {
            uploadList.push(res.result)
          }
        })

        if (uploadList.length === 0) {
          uploadList.push(res.result)
        }
        that.setData({
          uploadList: uploadList,
          num: uploadList.length
        })
      },
      fail: (res) => {
      }
    })
  },
  onDelete: function (e) {
    var that = this
    const id = e.currentTarget.dataset.id
    const uploadList = that.data.uploadList
    const newList = []
    uploadList && uploadList.map(item => {
      if (item != id) {
        newList.push(item)
      }
    })

    that.setData({
      uploadList: newList,
      num: newList.length
    })
  },
  onClear: function (e) {//清空
    var that = this
    that.setData({
      uploadList: [],
      num: 0
    })
  },
  onSubmit: function () {

    // let getMaterialList = app.globalData.getMaterialList
    // let that = this
    // let id = that.data.id
    // let type = that.data.title
    // let uploadList = that.data.uploadList
    // let pick_materiel_number = ''
    // uploadList && uploadList.map((item, index) => {
    //   if (index == uploadList.length - 1) {
    //     pick_materiel_number = pick_materiel_number + item
    //   } else {
    //     pick_materiel_number = pick_materiel_number + item + '、'
    //   }
    // })
    // let pick_amount = uploadList.length
    // let newItem = { id, type, pick_amount, pick_materiel_number, uploadList }

    // if (getMaterialList.length > 0) {
    //   console.log("1")

    //   getMaterialList && getMaterialList.map(item => {
    //     if (item.id != id) {
    //       getMaterialList.push(newItem)
    //       console.log("2")

    //     } else {
    //       console.log("3")
          
    //       item.pick_amount += pick_amount 
    //       item.pick_materiel_number = pick_materiel_number
    //       item.uploadList = uploadList
    //     }
    //   })
    // } else {
    //   console.log("4")

    //   getMaterialList.push(newItem)
    // }



    // // console.log("getMaterialList")
    // // console.log(getMaterialList)



    // app.globalData.getMaterialList = getMaterialList
    let that = this 
    function isNumber(obj){
      return obj === +obj;
    }
    const typeInfo = that.data.typeInfo
    console.log(typeInfo)

    const codeInfo = that.data.codeInfo
    console.log(codeInfo)

    const numberInfo = parseInt(that.data.numberInfo)
    console.log(numberInfo)

    let  Auxiliary = {
      typeInfo: typeInfo,
      codeInfo: codeInfo,
      numberInfo: numberInfo
    }
    console.log(Auxiliary)
    console.log(isNumber(numberInfo))
    if (typeInfo == '' || codeInfo == '' || numberInfo == ''){
      wx.showToast({
        title: '请填写完整',
        icon:"none",
        duration:1000
      })
    } else if (isNumber(numberInfo) == false){
      wx.showToast({
        title: '请正确填写',
        icon: "none",
        duration: 1000
      })
    }else{
      app.globalData.Auxiliary = Auxiliary
      wx.navigateBack({
        delta: 1
      })
      
    }

  }
})