// pages/personal/purchase/orderDetail/scrapPay/scrapPay.js]
import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    payInfoList: [],
    payInfoListClose: [],
    open: false,
    totalMoney: '',
    account: '',
    otherFeeList: '',
    accontInfo: {}
  },
  onlook: function (e) {
    wx.navigateTo({
      url: "../orderView/orderView?id=" + this.options.id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const id = options.id
    that.setData({id:id})
    query({
      url: 'api/waste/purchase/read',
      params: { id },
      back: function (data) {
        let item = data.result
        const orderStatus = item.state
        const photoList = item.cargo_images_domain
        const payInfoListClose = []
        payInfoListClose.push(item.pick_cargo_data.cargo_info)
        console.log(payInfoListClose)
        const totalMoney = (item.pick_cargo_data.cargo_price - item.pick_cargo_data.purchase_expense).toLocaleString()
        that.setData({
          id: id,
          item: item,
          totalMoney: totalMoney,
          payInfoList: item.pick_cargo_data.cargo_info,
          payInfoListClose: payInfoListClose,
          otherFeeList: item.pick_cargo_data.expense_info
        })
      }
    })


    //获取选项
    query({
      url: 'api/ajax/sys_config',
      params: {},
      back: function (data) {
        const pickList = data.result
        app.globalData.pickList = pickList
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this
    const id = that.options.id
    that.setData({ id: id })
    const accountName = app.globalData.accountName
    const accountId = app.globalData.accountId

    const orderDetail = app.globalData.orderDetail
    const oldList = app.globalData.oldList
    console.log("accountId")
    console.log(accountId)
    let accontInfo = {}
    oldList && oldList.map(item=>{
      if (item.id == accountId){
        accontInfo = item
      }
    })
    that.setData({
      account: accountName,
      accountId: accountId,
      accontInfo: accontInfo
    })
  },
  //点击跳转选择收款账户
  onpickman:function(){
    const that = this 
    console.log(that.id)
    wx.navigateTo({
      url: '../pickAccount/pickAccount?id=' +  that.options.id,
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onOpen: function () {
    const that = this
    that.setData({
      open: true
    })
  },
  onSubmit: function () {

    const that = this
    const id = that.data.id
    const gathering_id = that.data.accontInfo.id 

    if (!gathering_id) {
      wx.showToast({
        title: '请填写完整',
        icon: 'none',
        duration: 2000
      })
      return;
    }

    const data = {
      id,
      gathering_id
    }

    wx.showModal({
      title: '提示',
      content: '是否确认付款',
      success: function (res) {
        if (res.confirm) {
          create({
            url: 'api/waste/purchase/pay',
            params: { ...data },
            back: function (data) {
              if (data.code === 200) {
                console.log(111)
                wx.navigateTo({ url: '../paySuccess/paySuccess?id=' + id })
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    
  }
})