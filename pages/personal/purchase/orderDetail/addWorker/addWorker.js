// pages/personal/purchase/orderDetail/addWorker/addWorker.js

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  onname:function(e){
    console.log(e)
    const that = this
    const name = e.detail.value

    that.setData({
      name:name
    })
  },
  onphone: function (e) {
    console.log(e)
    const that = this
    const phone = e.detail.value

    that.setData({
      phone: phone
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  onSave: function () {
    // wx.navigateTo({ url: '/pages/personal/purchase/orderDetail/workers/workers' })
    const  that = this 
    const name = that.data.name 
    const phone = that.data.phone
    
    if(name == '' || phone == ''){
      wx.showToast({
        title: '请完整输入',
        icon:"none",
        duration:1000
      })
      return
    }
    let list = {}
    list.name = name
    list.phone = phone
    app.globalData.TemporaryWorker = list
    wx.navigateBack({
      delta: 1
    })

  }
})