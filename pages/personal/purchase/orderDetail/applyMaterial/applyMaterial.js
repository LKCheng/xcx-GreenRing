// pages/personal/purchase/orderDetail/applyMaterial/applyMaterial.js
import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    // open: '',
    materielList: [],
    warehouseName: '',
    warehouseId: '',
    startTime: '',
    date: '',
    time: '',
    message: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    //获取选项
    query({
      url: 'api/ajax/sys_config',
      params: {},
      back: function (data) {
        const pickList = data.result
        app.globalData.pickList = pickList
      
      }
    })

    var that = this
    const materielList = app.globalData.materielList

    const warehouseName = app.globalData.warehouseName
    const warehouseId = app.globalData.warehouseId

    // let open = true
    // if (boxNum == 0 && bagNum == 0 && glovesNum == 0 && masksNum == 0) {
    //   open = false
    // }

    //获取当前时间，格式YYYY-MM-DD
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
      month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
      strDate = "0" + strDate;
    }
    var startTime = year + seperator1 + month + seperator1 + strDate;

    that.setData({
      // boxNum: boxNum,
      // bagNum: bagNum,
      // glovesNum: glovesNum,
      // masksNum: masksNum,
      // open: open,
      materielList: materielList,
      warehouseName: warehouseName,
      warehouseId: warehouseId,
      startTime: startTime,
      id: options.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    const materielList = app.globalData.materielList
    const warehouseName = app.globalData.warehouseName
    const warehouseId = app.globalData.warehouseId

    // let open = true
    // if (boxNum == 0 && bagNum == 0 && glovesNum == 0 && masksNum == 0) {
    //   open = false
    // }

    //获取当前时间，格式YYYY-MM-DD
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
      month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
      strDate = "0" + strDate;
    }
    var startTime = year + seperator1 + month + seperator1 + strDate;

    that.setData({
      materielList: materielList,
      // open: open,
      warehouseName: warehouseName,
      warehouseId: warehouseId,
      startTime: startTime
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onAdd: function (e) {
    wx.navigateTo({
      url: "../pickMaterial/pickMaterial"
    })
  },
  addBox: function (e) {
    const that = this
    const key = e.currentTarget.dataset.key
    const materielList = that.data.materielList
    materielList && materielList.map(item => {
      if (item.key == key) {
        if (!item.num) {
          item.num = 1
        } else {
          item.num++
        }
      }
    })
    app.globalData.materielList = materielList

    that.setData({
      materielList: materielList
    })
  },
  cutBox: function (e) {
    const that = this
    const key = e.currentTarget.dataset.key
    const materielList = that.data.materielList
    materielList && materielList.map(item => {
      if (item.key == key) {
        item.num--
      }
    })
    app.globalData.materielList = materielList

    that.setData({
      materielList: materielList
    })
  },
  bindDateChange: function (e) {
    console.log(e.detail.value.length)
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },
  onLook: function () {
    wx.navigateTo({ url: '../orderView/orderView?id=' + this.data.id })
  },
  onBlur: function (e) {
    const that = this
    const value = e.detail.value
    that.setData({
      message: value
    })
  },
  onSubmit: function (e) {

    const _this = this.data

    const id = _this.id
    const materielList = _this.materielList
    const apply_materiel_warehouse = _this.warehouseId
    const date = _this.date
    const time = _this.time
    const apply_materiel_remark = _this.message
    const apply_materiel_picktime = date + ' ' + time
    const is_draft = 0
    
    const materiel_info = []
    materielList && materielList.map(item=>{
      if (item.num){
        const newItem = { type: item.value, amount: item.num }
        materiel_info.push(newItem)
      }
     
    })

    const data = {
      id,
      materiel_info,
      apply_materiel_warehouse,
      apply_materiel_picktime,
      apply_materiel_remark,
      is_draft
    }

    console.log(data)

 

    if (apply_materiel_warehouse == '' || date == '' || time == '') {
      wx.showToast({
        title: '请填写完整',
        icon: 'none',
        duration: 2000
      })
      return;
    }


    wx.showModal({
      title: '提示',
      content: '是否确认提交',
      success: function (res) {
        if (res.confirm) {
          create({
            url: 'api/waste/purchase/apply_materiel',
            params: { ...data },
            back: function (data) {
              if (data.code === 200) {
                wx.navigateTo({ url: '../applySuccess/applySuccess?id=' + id })
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },
})