// pages/personal/purchase/orderDetail/eva/eva.js
import { create, query } from '../../../../../utils/service.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    startListOne: [],
    startListTwo: [],
    startListThree: [],
    id: '', 
    message: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const startListOne = [{ id: 1, checked: false }, { id: 2, checked: false }, { id: 3, checked: false }, { id: 4, checked: false }, { id: 5, checked: false }]
    const startListTwo = [{ id: 1, checked: false }, { id: 2, checked: false }, { id: 3, checked: false }, { id: 4, checked: false }, { id: 5, checked: false }]
    const startListThree = [{ id: 1, checked: false }, { id: 2, checked: false }, { id: 3, checked: false }, { id: 4, checked: false }, { id: 5, checked: false }]
    that.setData({
      startListOne: startListOne,
      startListTwo: startListTwo,
      startListThree: startListThree,
      id: options.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onOk: function (e) {
    const that = this
    let type = e.currentTarget.dataset.num
    let id = e.currentTarget.dataset.id
    const startListOne = that.data.startListOne
    const startListTwo = that.data.startListTwo
    const startListThree = that.data.startListThree

    if (type == 'one') {
      startListOne.map(item => {
        if (item.id > id) {
          item.checked = false
        } else {
          item.checked = true
        }
      })
    }

    if (type == 'two') {
      startListTwo.map(item => {
        if (item.id > id) {
          item.checked = false
        } else {
          item.checked = true
        }

      })
    }

    if (type == 'three') {
      startListThree.map(item => {
        if (item.id > id) {
          item.checked = false
        } else {
          item.checked = true
        }
      })
    }
    that.setData({
      startListOne: startListOne,
      startListTwo: startListTwo,
      startListThree: startListThree
    })
  },
  onCancel: function (e) {
    const that = this
    let type = e.currentTarget.dataset.num
    let id = e.currentTarget.dataset.id
    const startListOne = that.data.startListOne
    const startListTwo = that.data.startListTwo
    const startListThree = that.data.startListThree

    if (type == 'one') {
      startListOne.map(item => {
        if (item.id >= id) {
          item.checked = false
        } else {
          item.checked = true
        }
      })
    }

    if (type == 'two') {
      startListTwo.map(item => {
        if (item.id >= id) {
          item.checked = false
        } else {
          item.checked = true
        }

      })
    }

    if (type == 'three') {
      startListThree.map(item => {
        if (item.id >= id) {
          item.checked = false
        } else {
          item.checked = true
        }
      })
    }
    that.setData({
      startListOne: startListOne,
      startListTwo: startListTwo,
      startListThree: startListThree
    })
  },
  onBlur: function (e) {
    const that = this
    const value = e.detail.value
    console.log(e)
    that.setData({
      message: value
    })
  },
  onSubmit: function () {

    const that = this
    const id = that.data.id
    const startListOne = that.data.startListOne
    const startListTwo = that.data.startListTwo
    const startListThree = that.data.startListThree
    const content = that.data.message

    console.log("startListOne")
    console.log(startListOne)

    let remove_fast_star = 0
    startListOne && startListOne.map(item=>{
      if(item.checked){
        remove_fast_star++
      }
    })

    let remove_level_star = 0
    startListTwo && startListTwo.map(item => {
      if (item.checked) {
        remove_level_star++
      }
    })

    let service_attitude_star = 0
    startListThree && startListThree.map(item => {
      if (item.checked) {
        service_attitude_star++
      }
    })

    const evaluate_info = { remove_fast_star, remove_level_star, service_attitude_star, content}
    const data = {
      id,
      evaluate_info
    }
    wx.showModal({
      title: '提示',
      content: '是否确认提交',
      success: function (res) {
        if (res.confirm) {
          create({
            url: 'api/waste/purchase/evaluate',
            params: { ...data },
            back: function (data) {
              if (data.code === 200) {
                wx.navigateTo({ url: '../evaSuccess/evaSuccess?id=' + id })
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  }
})