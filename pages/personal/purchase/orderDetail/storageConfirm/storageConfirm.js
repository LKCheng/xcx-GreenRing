import { create, query } from '../../../../../utils/service.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    ReValue:'',//留言信息
    item:{},
    GrossProfit:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this 
    console.log(options) 
    const id = options.id
    query({
      url: 'api/waste/purchase/read',
      params: { id },
      back: function (data) {
        const item = data.result

        console.log(item)

        //采购净重
        const allInfo = item.storage_sort_data.sort_diff
        let  cainet_wight = 0
        for (let i = 0; i < allInfo.length;i++){
          if (!allInfo[i].purchase_net_weight){
            allInfo[i].purchase_net_weight = 0
          }
          cainet_wight += allInfo[i].purchase_net_weight
        }
        //入库净重
        let runet_wight = 0
        for (let i = 0; i < allInfo.length; i++) {
          if (!allInfo[i].net_weight) {
            allInfo[i].net_weight = 0
          }
          runet_wight += allInfo[i].net_weight
        }
        //毛利润 GrossProfit
        let GrossProfit = that.data.GrossProfit
        let grosssss
        let priceList = item.storage_confirm_data
        grosssss =  priceList.total_presell_price - priceList.cargo_price + priceList.purchase_expense - priceList.return_fee - priceList.storage_price - priceList.sort_expense
        GrossProfit = Math.round(grosssss * 100) / 100
        
        that.setData({
          item:item,
          cainet_wight: cainet_wight,
          runet_wight: runet_wight,
          GrossProfit: GrossProfit
        })
      }
    })


    that.setData({
      id:id,
   
    })   
  },
  onRemake:function(e){
    const that = this 
    const ReValue = e.detail.value
    that.setData({
      ReValue: ReValue
    }) 
    console.log(ReValue)
  },
  onSubmit:function(){
    const that = this 
    const {id, ReValue} = that.data
    const data = {
      id:id,
      storage_confirm_remark:ReValue
    }
    wx.showModal({
      title: '提示',
      content: '是否确认提交',
      success:function(res){
        if(res.confirm){
          create({
            url:'api/waste/purchase/storage_confirm',
            params:{...data},
            back:function(data){
              if (data.code == 200){
                wx.navigateTo({
                  url: "../storageSuccess/storageSuccess?id=" + id 
                })
              }
            }
          })
        }else{
          console.log("用户点击取消")
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})