// pages/personal/purchase/orderDetail/pickUse/pickUse.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [{ name: "过磅费", id: 1 }, { name: "叉车费", id: 2 }, { name: "其他", id: 3 }],
    selectedId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    const status = options.status
    console.log("status");
    console.log(status);

    const useId = app.globalData.useId
    that.setData({
      selectedId: useId
    })

    // if (status == 'new') {
    //   that.setData({
    //     selectedId: ''
    //   })
    // } else {
    //   const payId = app.globalData.payId
    //   that.setData({
    //     selectedId: payId
    //   })
    // }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSelect: function (e) {
    var that = this
    const data = that.data
    const id = e.currentTarget.dataset.companyid
    const name = e.currentTarget.dataset.companyname
    const selectedId = that.data.selectedId

    if (id == selectedId) {
      that.setData({
        selectedId: ''
      })
      return;
    }
    that.setData({
      selectedId: id
    })

    app.globalData.useId = id
    app.globalData.useName = name
    wx.navigateBack({
      delta: 1
    })
    // wx.navigateTo({ url: '../addPay/addPay'}) 
  }
})