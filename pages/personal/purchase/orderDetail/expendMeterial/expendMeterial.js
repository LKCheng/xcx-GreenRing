// pages/personal/purchase/orderDetail/expendMeterial/expendMeterial.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    boxNum: 0,
    bagNum: 0,
    glovesNum: 0,
    masksNum: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    const boxNum = app.globalData.boxNum
    const bagNum = app.globalData.bagNum
    const glovesNum = app.globalData.glovesNum
    const masksNum = app.globalData.masksNum

    that.setData({
      boxNum: boxNum,
      bagNum: bagNum,
      glovesNum: glovesNum,
      masksNum: masksNum
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addBox: function () {
    var that = this
    let boxNum = that.data.boxNum
    boxNum++
    that.setData({
      boxNum: boxNum
    })
  },
  addGloves: function () {
    var that = this
    let glovesNum = that.data.glovesNum
    glovesNum++
    that.setData({
      glovesNum: glovesNum
    })
  },
  cutGloves: function () {
    var that = this
    let glovesNum = that.data.glovesNum
    glovesNum--
    that.setData({
      glovesNum: glovesNum
    })
  },
  addMasks: function () {
    var that = this
    let masksNum = that.data.masksNum
    masksNum++
    that.setData({
      masksNum: masksNum
    })
  },
  cutMasks: function () {
    var that = this
    let masksNum = that.data.masksNum
    masksNum--
    that.setData({
      masksNum: masksNum
    })
  },
  onSubmit: function () {
    var that = this.data
    // const boxNum = that.boxNum
    // const bagNum = that.bagNum
    const glovesNum = that.glovesNum
    const masksNum = that.masksNum

    // app.globalData.boxNum = boxNum
    // app.globalData.bagNum = bagNum
    app.globalData.glovesNumOut = glovesNum
    app.globalData.masksNumOut = masksNum

    // wx.navigateTo({ url: '/pages/personal/purchase/orderDetail/extractMaterial/extractMaterial' })

    wx.navigateBack({
      delta:1
    })
  }
})