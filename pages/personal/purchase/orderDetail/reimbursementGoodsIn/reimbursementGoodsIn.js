// pages/personal/purchase/orderDetail/reimbursementGoodsIn/reimbursementGoodsIn.js
import { create, query } from '../../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    close: false,
    reimbursementList: [],
    payTypeTotal: '',
    payTotal: '',
    useName:'',
    payeeNames:'',//人
    note:'',//留言
    payprice:'',//费用
    startX: 0, //开始坐标
    startY: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      const that  = this 
      const id = this.options.id
      that.setData({
        id:id
      })
      console.log(id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this
    const reimbursementList = app.globalData.reimbursementList
    const payTypeTotal = reimbursementList.length
    let price = ''
    const reiObj = reimbursementList[0]
    console.log(reiObj)
    if(reiObj != undefined){
      const payeeNames = reiObj.payeeName
      const useName = reiObj.useName
      const note = reiObj.note
      const payprice = reiObj.price
      that.setData({
        payeeNames: payeeNames,
        useName: useName,
        note: note,
        payprice: payprice
      })
    }
    reimbursementList && reimbursementList.map((item, index) => {
      price = price * 1 + item.price * 1,
      item.isTouchMove = false
      item.id = index
    })

    var payTotal = (price).toLocaleString()//合计的价钱
    //订单信息
    const id = that.options.id
    query({
      url:'api/waste/purchase/read',
      params:{ id },
      back:function(data){
        console.log(data)
        const item = data.result
        that.setData({
          item:item
        })
      }
    })

    that.setData({
      reimbursementList: reimbursementList,
      payTypeTotal: payTypeTotal,
      payTotal: payTotal,
      
    })
  //  console.log(reimbursementList)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onClose: function () {
    const that = this
    that.setData({
      close: true
    })
  },
  onAdd: function () {
    wx.navigateTo({
      url: '../reimbursementInfo/reimbursementInfo',
    })
  },
  onSubmit: function () {
    const  that  = this 
    const { id,useName, payeeNames, note, payprice} = this.data 
    const return_fee_info = [{usage:useName ,  remark: note , price: payprice , receiver:payeeNames}]
    console.log(return_fee_info)
    const data = {
        id:this.options.id,
        return_fee_info: return_fee_info,
        is_draft:0
    }
    //console.log(id)
    wx.showModal({
      title: '提示',
      content: "是否确认提交",
      success: function (res) {
        if (res.confirm) {
          create({
            url: 'api/waste/purchase/storage_connect_confirm',
            params: { ...data },
            back: function (data) {
              if (data.code === 200) {
                wx.navigateTo({
                  url: "../reimbursementSuccess/reimbursementSuccess?id=" + that.options.id
                })
              }
            }
          })
        } else {
          console.log('用户点击取消')
        }
      }
    })



    // wx.navigateTo({
    //   url: '../reimbursementSuccess/reimbursementSuccess',
    // })
  },
  //手指触摸动作开始 记录起点X坐标
  touchstart: function (e) {
    //开始触摸时 重置所有删除
    this.data.reimbursementList.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })
    this.setData({
      startX: e.changedTouches[0].clientX,
      startY: e.changedTouches[0].clientY,
      reimbursementList: this.data.reimbursementList
    })
  },
  //滑动事件处理
  touchmove: function (e) {

    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      startX = that.data.startX,//开始X坐标
      startY = that.data.startY,//开始Y坐标
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.reimbursementList.forEach(function (v, i) {
      v.isTouchMove = false
      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
    })
    //更新数据
    that.setData({
      reimbursementList: that.data.reimbursementList
    })
  },
  /**
   * 计算滑动角度
   * @param {Object} start 起点坐标
   * @param {Object} end 终点坐标
   */
  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  //删除事件
  del: function (e) {
    const that = this
    console.log(e)
    const id = e.currentTarget.dataset.id
    const list = that.data.reimbursementList
    let reimbursementList = []
    let price = ''
    list && list.map(item => {
      if (item.id != id) {
        reimbursementList.push(item)
        price = price * 1 + item.price * 1
      }
    })

    var payTotal = (price).toLocaleString()//合计的价钱
    const payTypeTotal = reimbursementList.length
    app.globalData.reimbursementList = reimbursementList

    that.setData({
      reimbursementList: reimbursementList,
      payTypeTotal: payTypeTotal,
      payTotal: payTotal
    })
  }
})