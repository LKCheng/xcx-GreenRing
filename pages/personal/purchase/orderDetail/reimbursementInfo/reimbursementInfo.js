// pages/personal/purchase/orderDetail/reimbursementInfo/reimbursementInfo.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    useName: '',
    payeeName: '',
    payeePosition: '',
    payeeId: '',
    price: '',
    note: '',
    useName:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    that.setData({
      useName: '',
      payeeName: '',
      payeePosition: '',
      payeeId: '',
      price: '',
      note: ''
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this
    const useName = app.globalData.useName

    const payeeId = app.globalData.payeeId
    const payeeName = app.globalData.payeeName
    const payeePosition = app.globalData.payeePosition

    that.setData({
      useName: useName,
      payeeName: payeeName,
      payeePosition: payeePosition,
      payeeId: payeeId
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onUse: function () {
    wx.navigateTo({
      url: '../pickUse/pickUse',
    })
  },
  payee: function (e) {
    console.log(e)
    const that = this 
    const payeeName = e.detail.value
    that.setData({
      payeeName: payeeName
    })
  },
  useInfo:function(e){
    const that = this
    const useName = e.detail.value
    that.setData({
      useName: useName
    })
  },
  onInput: function (e) {
    const that = this
    const inputType = e.currentTarget.dataset.name
    const value = e.detail.value
    if (inputType == 'price') {
      that.setData({
        price: value
      })
    }
    if (inputType == 'note') {
      that.setData({
        note: value
      })
    }
  },
  onSubmit: function () {
    const that = this.data
    const reimbursementList = app.globalData.reimbursementList
    const list = {}
    list.useName = that.useName
    list.payeeName = that.payeeName
    list.payeePosition = that.payeePosition
    list.payeeId = that.payeeId
    list.price = that.price
    list.note = that.note
    if (list.useName != "" && list.payeeName != "" && list.price != ""){
      reimbursementList.push(list)
      app.globalData.reimbursementList = reimbursementList
    }
    // 清空缓存
    app.globalData.payeeId = ''
    app.globalData.payeeName = ''
    app.globalData.payeePosition = ''
    app.globalData.useName = ''
    app.globalData.useId = ''
    console.log(list)
    if(list.useName === "" || list.payeeName === "" || list.price === ""){
      wx.showToast({
        title: '请填写完整',
        icon: 'none',
        duration: 1000
      })
    }else{
      wx.navigateBack({
         delta: 1
      })
    }
   

  }
})