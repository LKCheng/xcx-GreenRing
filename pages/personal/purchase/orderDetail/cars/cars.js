const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    selectedId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this

    const carId = app.globalData.carId
    const pickList = app.globalData.pickList

    const carList = pickList.car_type

    that.setData({
      list: carList,
      selectedId: carId
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSelect: function (e) {//选择地址
    var that = this
    const data = that.data
    const id = e.currentTarget.dataset.id
    const name = e.currentTarget.dataset.name

    const selectedId = that.data.selectedId
    if (id == selectedId) {
      that.setData({
        selectedId: ''
      })
      return;
    }
    that.setData({
      selectedId: id
    })

    app.globalData.carId = id
    app.globalData.carNum = name

    console.log("name")
    console.log(name)

    // wx.navigateTo({ url: '/pages/personal/purchase/orderDetail/assignOrder/assignOrder' })

    wx.navigateBack({
      delta: 1
    })


  }
})