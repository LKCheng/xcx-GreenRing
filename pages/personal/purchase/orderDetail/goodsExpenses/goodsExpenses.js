import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({
  data:{
    TYPEvalue:'',//开支类型
    PRvalue: '',//价格
    RMvalue: '',//留言
  },
  onshow:function(){
   
  },
  ontypeExpend: function (e) {
    const that = this
    const TYPEvalue = e.detail.value
    that.setData({
      TYPEvalue: TYPEvalue
    })
   },
  onprice:function(e){
    const that = this
    const PRvalue = e.detail.value
    {
      that.setData({
        PRvalue: PRvalue
      })
    }
  },
  onremark: function (e) { 
    const that = this
    const RMvalue = e.detail.value
    that.setData({
      RMvalue: RMvalue
    })
  },
  formSubmit:function(e){
    const that = this
    const list = app.globalData.expense_info 
    const { RMvalue, PRvalue, TYPEvalue} = that.data
    let reg = /^\d+$/ //验证输入的数字
    console.log(RMvalue, PRvalue, TYPEvalue)
    if (TYPEvalue === '' || PRvalue === ''){
      wx.showToast({
        title: '请完整填写',
        icon:'none',
        duration:1000
      })
    } else if (reg.test(PRvalue) == false)
    {
      wx.showToast({
        title: '请规范填写',
        icon: 'none',
        duration: 1000
      })
    } else{
      console.log(11)
      let expense_info = { usage: TYPEvalue, remark: RMvalue, price: PRvalue}
      list.push(expense_info)
      wx.navigateBack({
        delta:1
      })
    }
  }
})