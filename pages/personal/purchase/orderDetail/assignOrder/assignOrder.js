// pages/personal/purchase/orderDetail/assignOrder/assignOrder.js
import { create, query } from '../../../../../utils/service.js'
import { clear } from '../../../../../utils/clear.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',//订单id
    salesType: false,
    sortType: false,
    isNowBuySell:false,
    purchasingManagerId: '',
    purchasingManagerName: '',
    purchasingManagerCode: '',
    startTime: '',
    date: '',
    time: '',
    driverId: '',
    driverName: '',
    driverCode: '',
    carId: '',
    carNum: '',
    workerList: '',
    message: '',//留言
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    const purchasingManagerId = app.globalData.purchasingManagerId
    const purchasingManagerName = app.globalData.purchasingManagerName
    const purchasingManagerCode = app.globalData.purchasingManagerCode

    console.log(options)

    const driverId = app.globalData.driverId
    const driverName = app.globalData.driverName
    const driverCode = app.globalData.driverCode

    const carId = app.globalData.carId
    const carNum = app.globalData.carNum

    const workerList = app.globalData.workerList

    console.log("workerList")
    console.log(workerList)
    let workerInfo = ''
    const workerNum = workerList.length

     
    workerList && workerList.map((item, index) => {
      if (index < 2) {
        if (index == 1) {
          workerInfo = workerInfo + item.nickname + "..." + `  共(${workerList.length})人`
        } else {
          if (index == workerList.length - 1) {
            workerInfo = workerInfo + item.nickname + `  共(${workerList.length})人`
          } else {
            workerInfo = workerInfo + item.nickname + ","
          }
        }
      }
    })


    //获取当前时间，格式YYYY-MM-DD
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
      month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
      strDate = "0" + strDate;
    }
    var startTime = year + seperator1 + month + seperator1 + strDate;


    //获取选项
    query({
      url: 'api/ajax/sys_config',
      params: {},
      back: function (data) {
        const pickList = data.result
        app.globalData.pickList = pickList
      }
    })

    that.setData({
      purchasingManagerId: purchasingManagerId,
      purchasingManagerName: purchasingManagerName,
      purchasingManagerCode: purchasingManagerCode,
      driverId: driverId,
      driverName: driverName,
      driverCode: driverCode,
      startTime: startTime,
      workerInfo: workerInfo,
      workerNum: workerNum,
      carId: carId,
      carNum: carNum,
      id: options.id
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    var that = this
    const purchasingManagerId = app.globalData.purchasingManagerId
    const purchasingManagerName = app.globalData.purchasingManagerName
    const purchasingManagerCode = app.globalData.purchasingManagerCode

    const purchasingSalesleaderId = app.globalData.purchasingSalesleaderId
    const purchasingSalesleaderName = app.globalData.purchasingSalesleaderName
    const purchasingSalesleaderCode = app.globalData.purchasingSalesleaderCode

  

    const driverId = app.globalData.driverId
    const driverName = app.globalData.driverName
    const driverCode = app.globalData.driverCode

    const carId = app.globalData.carId
    const carNum = app.globalData.carNum

    const workerList = app.globalData.workerList

    let workerInfo = ''
    const workerNum = workerList.length
    workerList && workerList.map((item, index) => {
      if (index < 2) {
        if (index == 1) {
          workerInfo = workerInfo + item.nickname + "..." + `  共(${workerList.length})人`
        } else {
          if (index == workerList.length - 1) {
            workerInfo = workerInfo + item.nickname + `  共(${workerList.length})人`
          } else {
            workerInfo = workerInfo + item.nickname + ","
          }
        }
      }
    })


    //获取当前时间，格式YYYY-MM-DD
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
      month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
      strDate = "0" + strDate;
    }
    var startTime = year + seperator1 + month + seperator1 + strDate;


    that.setData({
      purchasingManagerId: purchasingManagerId,
      purchasingManagerName: purchasingManagerName,
      purchasingManagerCode: purchasingManagerCode,
      purchasingSalesleaderId: purchasingSalesleaderId,
      purchasingSalesleaderName: purchasingSalesleaderName,
      purchasingSalesleaderCode: purchasingSalesleaderCode,
      driverId: driverId,
      driverName: driverName,
      driverCode: driverCode,
      startTime: startTime,
      workerInfo: workerInfo,
      workerNum: workerNum,
      carId: carId,
      carNum: carNum,
      workerList: workerList
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  changeSalesType: function (e) {
    const checked = e.detail.value
    let isNowBuySell = this.data.isNowBuySell
    this.setData({
      salesType: checked,
      isNowBuySell:checked
    })
  },
  changeSoryType: function (e) {
    const checked = e.detail.value
    this.setData({
      sortType: checked
    })
  },
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },
  onBlur: function (e) {
    const that = this
    const value = e.detail.value
    console.log(e)
    that.setData({
      message: value
    })
  },
  onSubmit: function (e) {

    const that = this
    const _this = this.data

    console.log(_this.workerList)

    const date = _this.date
    const time = _this.time

    const id = _this.id
    console.log(_this.salesType)
    const hand_mouth_data = _this.salesType == true ? 1 : 0 //现买现卖
    const locale_sort_data = _this.sortType == true ? 1 : 0 //现场分拣
    const purchase_incharge = _this.purchasingManagerId
    console.log(hand_mouth_data)
    let sell_incharge
    if (hand_mouth_data == 1){
       sell_incharge = _this.purchasingSalesleaderId
    } else if (hand_mouth_data == 0){
      sell_incharge = ''
    }
    
    const driver_id = _this.driverId
    const car_type = _this.carId
    const purchase_time = _this.date + ' ' + _this.time
    const workerList = _this.workerList
    const floater_ids = []
    workerList && workerList.map(item => {
      floater_ids.push(item.id)
    })
    const allot_remark = _this.message
    if (hand_mouth_data == 0){
      if (purchase_incharge == '' || driver_id == '' || car_type == '' || date == '' || time == '') {
        wx.showToast({
          title: '请填写完整',
          icon: 'none',
          duration: 1000
        })
        return;
      }
    }
    if (hand_mouth_data == 1) {
      if (purchase_incharge == '' || sell_incharge == '' || date == '' || time == '') {
        wx.showToast({
          title: '请填写完整',
          icon: 'none',
          duration: 1000
        })
        return;
      }
    }

    const is_draft = 0
    const data = {
      id,
      hand_mouth_data,
      locale_sort_data,
      purchase_incharge,
      sell_incharge,
      purchase_time,
      floater_ids,
      driver_id,
      car_type,
      allot_remark,
      is_draft
    }
    console.log(data)
    wx.showModal({
      title: '提示',
      content: '是否确认提交',
      success: function (res) {
        if (res.confirm) {
          create({
            url: 'api/waste/purchase/allot',
            params: { ...data },
            back: function (data) {
              
              if (data.code === 200) {
                clear()
                console.log(111)
                wx.navigateTo({ url: '../assignSuccess/assignSuccess?id=' + that.options.id })
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },
  onLook: function () {
    wx.navigateTo({ url: '../orderView/orderView?id=' + this.data.id })
  }
})