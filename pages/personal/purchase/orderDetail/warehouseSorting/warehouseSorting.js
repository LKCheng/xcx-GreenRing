import { create, query } from '../../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */

  data: {
    sorting_list:[],
    childe_list:[],
    sortingId:'',
    list: [],
    sorting_id:'',
    dataId:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this

  },
  onClicked:function(e){
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (e) {
    const that = this 
    query({
      url: 'api/ajax/warehouse_tree',
      params: { },
      back: function (data) {
        console.log(data)
        const sorting_list = data.result
        if (sorting_list && sorting_list.length > 0){
          that.setData({
            sorting_list:sorting_list
            //sortingId:sortingId
          })
        }
        console.log(sorting_list)
      }
    })
  },
  onClicked:function(e){
    const that = this
    console.log(e)
    const dataId = e.currentTarget.dataset.key
    app.globalData.sorting_id = dataId
    console.log(dataId)
    const sorting_list = this.data.sorting_list
    for (let i = 0; i < sorting_list.length;i++){
      //console.log(sorting_list[i].id)
      if (sorting_list[i].id === dataId){
          that.setData({
            dataId : dataId
          })
          wx.navigateTo({
            url: '../sortingPoint/sortingPoint',
          })
      }
    }
   
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSelect: function (e) {
    var that = this
    

  }
})
