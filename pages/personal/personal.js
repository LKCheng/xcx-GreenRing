// pages/personal/personal.js
import { Api } from './getInfo.js';
import { create, query } from '../../utils/service.js'
var api = new Api()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    logined: true,
    purchHidden: true,
    salesHidden: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  clickme: function () {
    wx.navigateTo({
      url: './bindAccount/bindAccount',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this
    this._getUserInfo()
    var admin_info = wx.getStorageSync('admin_info')
    that.setData({
      admin_info: admin_info
    })
    query({
      url: 'api/user/menu',
      params: {},
      back: function (data) {
        const menuList = data.result
        let menuArr = []
        let salesHidden = that.data.salesHidden
        let purchHidden = that.data.purchHidden

        if (menuList  &&   menuList.length > 0) {

          for (let i = 0; i < menuList.length; i++) {
            menuArr.push(menuList[i].key)

          }
        }
        if (menuArr.indexOf(299) < 0) {
          purchHidden = false
        }else{
          purchHidden = true
        }
        if (menuArr.indexOf(303) < 0) {
          salesHidden = false
        }else{
          salesHidden = true
        }
        console.log(purchHidden , salesHidden)
        that.setData({
          menuList: menuList,
          salesHidden: salesHidden,
          purchHidden: purchHidden
        })
      }
    })

  },

  /**
  * 获取用户数据
  */
  _getUserInfo() {
    let that = this
    api.getUserInfo((data) => {
      console.log(data)
      that.setData({
        userInfo: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onShow()
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})