
class Api{

  // 获取用户信息
  getUserInfo(cb) {
    let that = this
    wx.login({
      success(res) {
        wx.request({
          method: 'POST',
          url: 'https://testadmin.lhdrr.com/api/user/wxapp_login',
          data: {
            appletid: 'c948ca11073112af6147a25e9db6ed10',
            code: res.code,
          },
          success: function (res) {
            const sessionId = res.data.result.session_id
            wx.setStorageSync('sessionId', sessionId)
            const code = res.data.code
            //如果没绑定则前往绑定账号
            console.log(code)
            if (code === 1001) {
              wx.navigateTo({
                url: './bindAccount/bindAccount',
              })
              return;
            }
            const branch_text = res.data.result.admin_info.branch_text ? res.data.result.admin_info.branch_text : ''
            const branch_id = res.data.result.admin_info.branch_id ? res.data.result.admin_info.branch_id : ''
            
            wx.setStorageSync('branch_text', branch_text)
            wx.setStorageSync('branch_id', branch_id)
            wx.setStorageSync('admin_info', res.data.result.admin_info)
           
          }
        })
        //获取用户信息
        wx.getUserInfo({
          success(res) {
            typeof cb === 'function' && cb(res.userInfo)
            const data = JSON.parse(res.rawData)
            console.log(res)
            // wx.request({
            //   method:'POST',
            //   url: 'https://testadmin.lhdrr.com/api/user/login',
            //   data: {
            //     username: 'admin',
            //     password: 'uctoo123',
            //     keeplogin: 1
            //   },
            //   header: {
               
            //   },
            //   success: function (res) {
             
            //     console.log(res.data)
            //   }
            // })
          },
          fail() {
            typeof cb === 'function' && cb({
              // avatarUrl: '../../../images/icon/user@default.png',
              // nickName: '绿环'
            })
          }
        })
      }
    })
  }
}

export { Api }