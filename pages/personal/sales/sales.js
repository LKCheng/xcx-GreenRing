// pages/personal/sales/sales.js
import { create, query } from '../../../utils/service.js'
var cityData = require('../../../utils/city.js');
const app = getApp()
// page/one/index.js
Page({
  data: {
    list: [],//订单列表
    content: [],
    orderList: [],//状态列表
    operaText:'',
    px: [],
    qyopen: false,
    nzopen: false,
    pxopen: false,
    nzshow: true,
    pxshow: true,
    qyshow: true,
    isfull: false,
    empty: false,
    cityleft: cityData.getCity(),
    citycenter: {},
    cityright: {},
    province: '',
    orderId: '',//查询的订单id
    Searched: false,//是否被搜索过
    city: '',
    district: '',
    shownavindex: '',
    order: '',
    orderValue:'',
    districtId:'',
    branchList: [],
  },
  onLoad: function () {
    
  },
  onShow:function(){
    //获取订单的列表
    const that = this
    let cargo_info_list_len = app.globalData.cargo_info_list_len
    const states = that.data.orderId
    query({
      url: 'api/waste/sell/index',
      params: { states},
      back: function (data) {
        console.log(data)
        if (data.code === 400 || data.msg == '没有更多数据了。' ) {
          that.setData({
            empty: true
          })
          return;
        }
       
        const operate_add = data.operate_add
        const list = data.rows
        const orderList = data.state_search
        console.log(orderList)
        const newList = [{ value: '全部订单', key: '' }]
        orderList && orderList.map(item => {
          const orderItem = {}
          orderItem.value = item.text
          orderItem.key = item.value
          newList.push(orderItem)
        })


       
        console.log(list)
        console.log("list")
        let item 
        if (list){
          for (let i = 0; i < list.length; i++) {
            item = list[i]
            let operates = item.operates
            if (operates){
              for (let j = 0; j < operates.length; j++) {
                if (operates.length == 2) {
                  if (operates[j].text == '交易完成') {
                    operates[j].text = '详情'
                  }
                }
              }
            }
           
          }
        }
        
        
        that.setData({
          list: list,
          empty:false,
          current:1,
          operate_add: operate_add,
          orderList: newList,
          cargo_info_list_len: cargo_info_list_len
        })
      }

    })
    //获取订单详细信息
    let branch_text = wx.getStorageSync('branch_text');
    let branch_id = wx.getStorageSync('branch_id');
    if (branch_text) {
      const branchList = [{ value: branch_text, key: branch_id }]
      that.setData({
        branchList: branchList
      })
    } else {
      //获取选项
      query({
        url: 'api/ajax/sys_config',
        params: {},
        back: function (data) {
          const pickList = data.result
          app.globalData.pickList = pickList
          const branchList = pickList.branch
          // const newList = [{ value: '全部区域', key: '' }]
          //   newList.push(branchList)
          //   console.log("newList")
          //   console.log(newList)
          that.setData({
            branchList: branchList
          })
          console.log(branchList)
          
        }
      })
    }
  },
 
  listqy: function (e) {

    var that = this
    if (that.data.nzopen) {
      that.setData({
        nzopen: false,
        pxopen: false,
        qyopen: false,
        nzshow: false,
        pxshow: true,
        qyshow: true,
        isfull: false,
        shownavindex: 0
      })
      that.hidebg()
    } else {
      that.setData({
        content: that.data.branchList,
        nzopen: true,
        pxopen: false,
        qyopen: false,
        nzshow: false,
        pxshow: true,
        qyshow: true,
        isfull: true,
        shownavindex: e.currentTarget.dataset.nav
      })
    }

  },
  list: function (e) {
    var that = this
    if (that.data.nzopen) {
      that.setData({
        nzopen: false,
        pxopen: false,
        qyopen: false,
        nzshow: false,
        pxshow: true,
        qyshow: true,
        isfull: false,
        shownavindex: 0
      })
      that.hidebg()
    } else {
      that.setData({
        content: that.data.orderList,
        nzopen: true,
        pxopen: false,
        qyopen: false,
        nzshow: false,
        pxshow: true,
        qyshow: true,
        isfull: true,
        shownavindex: e.currentTarget.dataset.nav
      })
    }
  },
  listpx: function (e) {
    if (this.data.pxopen) {
      this.setData({
        nzopen: false,
        pxopen: false,
        qyopen: false,
        nzshow: true,
        pxshow: false,
        qyshow: true,
        isfull: false,
        shownavindex: 0
      })
    } else {
      this.setData({
        content: this.data.px,
        nzopen: false,
        pxopen: true,
        qyopen: false,
        nzshow: true,
        pxshow: false,
        qyshow: true,
        isfull: true,
        shownavindex: e.currentTarget.dataset.nav
      })
    }
    console.log(e.target)
  },
  selectleft: function (e) {
    console.log("e")
    console.log(e)
    var that = this
    if (e.target.dataset.city == '全部') {
      that.hidebg()
      that.setData({
        district: e.target.dataset.city
      })
    }
    that.setData({
      cityright: {},
      citycenter: that.data.cityleft[e.currentTarget.dataset.city],
      province: e.target.dataset.city,
      city: ''
    });
  },
  selectcenter: function (e) {

    this.setData({
      cityright: this.data.citycenter[e.currentTarget.dataset.city],
      city: e.target.dataset.city
    });
  },
  // hidebg: function (e) {

  //   this.setData({
  //     qyopen: false,
  //     nzopen: false,
  //     pxopen: false,
  //     nzshow: true,
  //     pxshow: true,
  //     qyshow: true,
  //     isfull: false,
  //     shownavindex: 0
  //   })
  // },
  hidebg: function (e) {

    this.setData({
      qyopen: false,
      nzopen: false,
      pxopen: false,
      nzshow: true,
      pxshow: true,
      qyshow: true,
      isfull: false,
      shownavindex: 0
    })
  },
  onSelectCity: function (e) {
    // 选择的省市区
    const that = this

    const shownavindex = that.data.shownavindex

    if (shownavindex == 1) {

      const order = e.target.dataset.order
      const orderId = e.target.dataset.id

      that.setData({
        order: order,
        orderId: orderId
      })
    }
console.log(e)
    if (shownavindex == 2) {
      const district = e.target.dataset.order
      const districtId = e.target.dataset.id
      that.setData({
        district: district,
        districtId: districtId
      })
    }
    that.hidebg()
  },
  selectOrder: function (e) {
    var that = this
    const order = e.target.dataset.order
    const value = e.target.dataset.value
    that.setData({
      order: order,
      orderValue:value
    })
    that.hidebg()
  },
  addNew: function (e) {
    wx.navigateTo({
      url: "./addSalesOrder/addSalesOrder"
    })
  },
  onSearch: function () {
    const that = this
    const states = that.data.orderId
    const branch_id = that.data.districtId
    console.log(states, branch_id)
    query({
      url: 'api/waste/sell/index',
      params: { states, branch_id},
      back: function (data) {
        let list = data.rows
        const orderList = data.state_search
        const newList = []
        orderList && orderList.map(item => {
          const orderItem = {}
          orderItem.value = item.text
          orderItem.key = item.value
          newList.push(orderItem)
        })
        if (data.code === 400 || !list) {
          list = []
          that.setData({
            list: list,
            empty: true,
            current: 1
          })
          return;
        }
        that.setData({
          list: list,
          current: 1,
          empty: false,
          Searched: true
        })
      }
    })
  },
  onClick:function(e){
    console.log(e)
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: "./salesView/salesView?id=" + id
    })
  },
  default:function(e){
    console.log(e)
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: "./salesView/salesView?id=" + id
    })
  },
  onReachBottom() {
    const that = this
    const current = that.data.current
    const oldList = that.data.list
    const Searched = that.data.Searched
    if (Searched === false) {
      wx.showNavigationBarLoading() //在标题栏中显示加载
      that.setData({
        showTip: true
      })
      query({
        url: 'api/waste/sell/index',
        params: { page: current + 1 },
        back: function (data) {
          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
          const code = data.code
          if (code === 200) {
            let list = data.rows
            list && list.map(item => {
              oldList.push(item)
            })
            // wx.showToast({
            //   title: data.msg,
            //   icon: 'success',
            //   duration: 2000
            // });
            that.setData({
              // orderList: ['全部订单', '订单1', '订单1', '订单1', '订单1', '订单1', '订单1'],
              list: oldList,
              current: current + 1
            })
          }
          if (code === 400) {
            const tip = data.msg
            wx.showToast({
              title: data.msg,
              icon: 'none',
              duration: 2000
            });
          }

        }
      })
    }


  },
  //下啦刷新
  onPullDownRefresh: function () {
    this.onShow()
    wx.stopPullDownRefresh()
  },
})
