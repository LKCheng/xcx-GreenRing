// pages/personal/purchase/orderDetail/orderDetail.js
import { create, query } from '../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',//订单的id
    firstStep:"",
    secondStep:"", 
    thirdStep: "",
    salseTotalPrice:'',
    metroPrice:'',
    otherPrice:'',
    sence_img:'',
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const id = options.id //id查询订单明细
    query({
      url:"api/waste/sell/read?id="+id,
      params: {},
      back:function(data){
        const purchase_info = data.purchase_info
        console.log(data)
        //现买现卖堆放方式
        if (purchase_info && purchase_info.cargo_images_domain) {
          const sence_img = purchase_info.cargo_images_domain
          that.setData({
            sence_img: sence_img
          })
        }
        //是否现买现卖
        if (purchase_info && purchase_info.hand_mouth_data == '1') {
          const is_hand_mouth_data = '是'
          that.setData({
            is_hand_mouth_data: is_hand_mouth_data
          })
        }
         //下单人信息
        if (purchase_info){
          const SinglePerson = purchase_info.seller_info
          app.globalData.SinglePerson = SinglePerson
        }
      
        
        let item = data.result
        console.log(item)
        let viewState = item.state
        let orderStatus = item.state
        //时间轴
        let timeList = item.timeline
        let firstStep, secondStep, thirdStep
        for (let i = 0; i < timeList.length;i++){
          if (viewState == timeList[i].remark){
            firstStep = timeList[i-1]
            secondStep = timeList[i]
            thirdStep = timeList[i+1]
          }
        }
        //货品数量
        let cargo_info_list = item.cargo_info
        let totalMoneyList = []
        let totalMoney
        if (item.cargo_info && item.cargo_info.length>0){
          for (let i = 0; i < cargo_info_list.length; i++) {
            totalMoneyList.push(cargo_info_list[i].plan_sell_weight * cargo_info_list[i].unit_price)
          }
          app.globalData.cargo_info_list_len = cargo_info_list.length
          for (let j = 0; j < totalMoneyList.length; j++) {
            if (totalMoneyList.length = 1) {
              totalMoney = totalMoneyList[j]
            } else if (totalMoneyList[j].length > 1) {
              totalMoney += totalMoneyList[j]
            }
          }
        }
        //在售货品合计
        let itemPrice,salseTotalPrice = 0,list = []
        if (item.cargo_info &&  item.cargo_info.length>0){
          for (let i = 0, len = item.cargo_info; i < len.length; i++) {
            itemPrice = len[i].net_weight * len[i].unit_price
            list.push(itemPrice)
          }
          for (let j = 0; j < list.length; j++) {
            salseTotalPrice += list[j]
          }
        }
        
        //辅材合计价格
        let mtroPrice, metroPrice = 0, mtrolist = []
        if (item.weigh_data && item.weigh_data.materiel_info){
          for (let i = 0, len = item.weigh_data.materiel_info; i < len.length; i++) {
            mtroPrice = len[i].pick_amount * len[i].unit_price
            mtrolist.push(mtroPrice)
          }
          for (let j = 0; j < mtrolist.length; j++) {
            metroPrice += mtrolist[j]
          }
        }
        
        //其他费用合计
        // otherPrice
        let othPrice, otherPrice = 0, otherlist = []
        if (item.weigh_data && item.weigh_data.other_price_info){
          for (let i = 0, len = item.weigh_data.other_price_info; i < len.length; i++) {
            othPrice = len[i].price
            otherlist.push(othPrice)
          }
          for (let j = 0; j < otherlist.length; j++) {
            otherPrice += otherlist[j]
          }

        }
        


        that.setData({
          id:id,
          item:item,
          purchase_info: purchase_info,
          firstStep: firstStep,
          secondStep: secondStep,
          thirdStep: thirdStep,
          cargo_info_list:cargo_info_list,
          orderStatus: orderStatus,
          totalMoney: totalMoney,
          salseTotalPrice: salseTotalPrice,
          metroPrice: metroPrice,
          otherPrice: otherPrice,
        
        })  
      }
    })
   
   
  },
  // clickimg: function (e) {
  //   var index = e.currentTarget.dataset.index

  //   const cargo_img = this.data.cargo_img
  //   this.previewImage(index, cargo_img)
  // },
  clickSceneImg: function (e) {
    
    var index = e.currentTarget.dataset.index
    const sence_img = this.data.sence_img
    this.previewImage(index, sence_img)
    
  },
  previewImage: function (index, imgArr) {

    wx.previewImage({
      current: imgArr[index],     //当前图片地址
      urls: imgArr,               //所有要预览的图片的地址集合 数组形式
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onPageJump: function (e) {
    const that = this
    const id = that.data.id
    console.log(11)
    console.log(e)
    const orderStatus = e.currentTarget.dataset.state
    switch (orderStatus) {
      case 'wait_weigh'://称重
        that.onWeigh();
        break;
      case 'wait_confirm_order'://确认订单
        that.onConfirmOfOrder();
        break; 
      case 'wait_commit_order'://销售经理（提交订单）
        that.placeOrder();
        break;
      case 'draft'://销售经理（提交订单）
        that.onPerfectInformation();
        break;
      case 'wait_pay'://付款
        that.onPayment();
        break;  
      case 'wait_confirm_gather'://确认收款
        that.onReceivables();
        break;   
      case 'finish'://交易完成
        that.onComplete();
        break;    
      // case 'wait_confirm_order'://评价
      //   that.onEvaluate();
      //   break;  
    }
    const flish = e.currentTarget.dataset.states
    if (flish == "finish"){
      that.onEvaluate();
    }
  },
  onWeigh:function(){
    wx.navigateTo({
      url: '../Weigh/Weigh?id='+this.data.id,
    })
  },
  placeOrder:function(){
   
    wx.navigateTo({
      url: '../placeOrder/placeOrder?id=' + this.data.id ,
    })
  },
  onConfirmOfOrder:function(){
    const that = this 
    const id = that.options.id 
    wx.showModal({
      title: '提示',
      content: '是否确认提交',
      success: function (res) {
        if (res.confirm){
          create({
            url: 'api/waste/sell/confirm_order',
            params: { id },
            back: function (data) {
              console.log(data)
              wx.navigateTo({
                url: '../confirmSuccess/confirmSuccess?id='+ that.options.id,
              })
            }
          })
        }
      }
    })
  },
  onPayment:function(){
    const that = this
    wx.navigateTo({
      url: '../scrapPay/scrapPay?id=' + that.options.id,
    })
  },
  onReceivables:function(){
    const that = this
    const id = that.options.id
    wx.showModal({
      title: '提示',
      content: '是否确认收款',
      success: function (res) {
        if (res.confirm) {
          create({
            url: 'api/waste/sell/confirm_gather',
            params: { id },
            back: function (data) {
              console.log(data)
              wx.navigateTo({
                url: '../confirmReceivablesSuccess/confirmReceivablesSuccess?id=' + that.options.id,
              })
            }
          })
        }
      }
    })
  },
  onEvaluate:function(){
    const that = this
    wx.navigateTo({
      url: '../eva/eva?id=' + that.options.id,
    })
  },
  onPerfectInformation:function(){
    const that = this 
    wx.navigateTo({
      url: '../PerfectInformation/PerfectInformation?id=' + that.options.id,
    })
  }
})