// pages/personal/purchase/orderDetail/addWorker/addWorker.js
import { create, query } from '../../../../../utils/service.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cargo_info: '',
    thisInfo:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    console.log(options)
    const id = options.id
    let thisInfo
    const info = app.globalData.PerfectInformation_info
    console.log(info)
    for (let i = 0; i < info.length;i++){
      if(id  == info[i].cate_id){
        thisInfo = info[i]
        
      }
    }
    that.setData({
      thisInfo: thisInfo
    })
    console.log(thisInfo)
  },
 
  onblur:function(e){
    const that = this 
    console.log(e)
    that.setData({
      input:e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    app.globalData.perfectInfo = ''
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSave: function () {
    const that = this
    const input = that.data.input
    function isNumber(obj){
      return obj === +obj
    }

    if (input == '' ) {
      wx.showToast({
        title: '请完整输入',
        icon: "none",
        duration: 1000
      })
      return
    } else if (isNumber(Number(input)) == false){
      wx.showToast({
        title: '请正确输入',
        icon: "none",
        duration: 1000
      })
      return
    }
    let thisInfo = that.data.thisInfo
    
    thisInfo.unit_price = Number(input)
    app.globalData.perfectInfo = thisInfo
    wx.navigateBack({
      delta: 1
    })

  }
})