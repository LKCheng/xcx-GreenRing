// pages/personal/purchase/orderDetail/addWorker/addWorker.js
import { create, query } from '../../../../utils/service.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cargo_info: '',
    allPriceList: [],
    allIdList: [],
    allPrice:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    console.log(options)
    const id = options.id
    query({
      url: "api/waste/sell/read?id=" + id,
      params: {},
      back: function (data) {
        console.log(data)
        const cargo_info = data.result.cargo_info
        app.globalData.PerfectInformation_info = cargo_info
        that.setData({
          cargo_info: cargo_info
        })
        console.log(cargo_info)
      }
    })
  },
  click: function (e) {
    const that = this
    console.log(e)
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: './info/info?id=' + id,
    })
  },
  onAddPay: function () {
    const status = 'new'
    wx.navigateTo({
      url: './addPay/addPay?status=' + status,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const perfectInfo = app.globalData.perfectInfo
    console.log(perfectInfo)
    let cargo_info = this.data.cargo_info
    let totalPrice
    let allPrice = this.data.allPrice
    let allPriceList = this.data.allPriceList, dataInfo = {}
    let allIdList = this.data.allIdList
    //各自总价
    if (perfectInfo) {
      for (let i = 0; i < cargo_info.length; i++) {
        if (cargo_info[i].cate_text == perfectInfo.cate_text) {
          cargo_info[i].unit_price = perfectInfo.unit_price
          totalPrice = cargo_info[i].unit_price * cargo_info[i].net_weight
          cargo_info[i].totalPrice = totalPrice
          console.log(totalPrice)
        }
        
        
      }
      
    }
   //合计
    if(perfectInfo){
      allPrice = 0
      for(let i=0;i<cargo_info.length;i++){
        console.log(cargo_info[i].totalPrice)
        if (!cargo_info[i].totalPrice){
          cargo_info[i].totalPrice = 0
        }
        allPrice += cargo_info[i].totalPrice
      }
    }

    //开支信息
    const salesPayInfoList = app.globalData.salesPayInfoList
    console.log(salesPayInfoList)
    let payTotal = 0
    salesPayInfoList.length > 0 && salesPayInfoList.map(payItem => {
      const money = payItem.price * 1
      payTotal = (payTotal * 1 + money * 1).toFixed(2)
    })

    this.setData({
      totalPrice: totalPrice,
      cargo_info: cargo_info,
      allPrice: allPrice,
      allPriceList: allPriceList,
      salesPayInfoList: salesPayInfoList,
      payTotal: payTotal
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSubmit: function () {
    // wx.navigateTo({ url: '/pages/personal/purchase/orderDetail/workers/workers' })
    const that = this
    const is_cargo_info = that.data.cargo_info
    console.log(is_cargo_info)
    let salesPayInfoList = that.data.salesPayInfoList
    const totalPrice = that.data.totalPrice
    is_cargo_info && is_cargo_info.map(item => {
      delete item.plan_sell_weight
      delete item.rough_weight
      delete item.total_weight
    })
   
    const data = {
      is_draft: 0,
      cargo_info: is_cargo_info,
      id: Number(that.options.id),
      other_price_info: salesPayInfoList

    }
    console.log(data)
    
    for (let i = 0; i < is_cargo_info.length; i++) {
      if (!is_cargo_info[i].unit_price) {
        wx.showToast({
          title: '请输入货品单价',
          icon: "none",
          duration: 1000
        })
        return
      }
    }
    create({
      url: "api/waste/sell/add",
      params: { ...data },
      back: function (data) {
        if (data.code === 200) {
          wx.navigateTo({
            url: './PerfectSuccess/PerfectSuccess?id=' + that.options.id,
          })
        }
      }
    })
    console.log(data)


  },
  //手指触摸动作开始 记录起点X坐标 --------删除货品
  touchstartPay: function (e) {
    //开始触摸时 重置所有删除
    this.data.salesPayInfoList.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })
    this.setData({
      startX: e.changedTouches[0].clientX,
      startY: e.changedTouches[0].clientY,
      salesPayInfoList: this.data.salesPayInfoList
    })
  },
  //滑动事件处理
  touchmovePay: function (e) {
    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      startX = that.data.startX,//开始X坐标
      startY = that.data.startY,//开始Y坐标
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.salesPayInfoList.forEach(function (v, i) {
      v.isTouchMove = false
      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
    })
    //更新数据
    that.setData({
      salesPayInfoList: that.data.salesPayInfoList
    })
  },
  /**
   * 计算滑动角度
   * @param {Object} start 起点坐标
   * @param {Object} end 终点坐标
   */
  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  //删除事件  
  delPay: function (e) {
    this.data.salesPayInfoList.splice(e.currentTarget.dataset.index, 1)


    let salesPayInfoList = this.data.salesPayInfoList
    let payTotal = this.data.payTotal
    let money = [], newNumber = 0
    for (let i = 0; i < salesPayInfoList.length; i++) {
      money.push(Number(salesPayInfoList[i].price))
    }
    for (let j = 0; j < money.length; j++) {
      newNumber += money[j]
    }
    payTotal = newNumber.toFixed(2)
    console.log(payTotal)

    this.setData({
      salesPayInfoList: this.data.salesPayInfoList,
      payTotal: payTotal
    })
    app.globalData.salesPayInfoList = this.data.salesPayInfoList
  },
})