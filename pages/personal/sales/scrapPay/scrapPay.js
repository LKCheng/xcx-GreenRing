// pages/personal/purchase/orderDetail/scrapPay/scrapPay.js]
import { create, query } from '../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    payInfoList: [],
    payInfoListClose: [],
    open: false,
    totalMoney: '',
    account: '',
    otherFeeList: '',
    accontInfo: {},
    payArray:[],
    otherPriceList:[],
    otherPriceListClose:[],
  },
  onlook: function (e) {
    wx.navigateTo({
      url: "../orderView/orderView?id=" + this.options.id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const id = options.id
    that.setData({id:id})
    query({
      url: 'api/waste/sell/read',
      params: { id },
      back: function (data) {
        let item = data.result
        console.log(item)
        const orderStatus = item.state
        const payInfoListClose = []
        let  otherPriceList = that.data.otherPriceList
        otherPriceList = item.weigh_data.other_price_info
        const otherPriceListClose = []
        //在售货品合计
        let itemPrice, salseTotalPrice = 0, list = []
        if (item.cargo_info.length > 0) {
          for (let i = 0, len = item.cargo_info; i < len.length; i++) {
            itemPrice = len[i].net_weight * len[i].unit_price
            list.push(itemPrice)
          }
          for (let j = 0; j < list.length; j++) {
            salseTotalPrice += list[j]
          }
        }

        //辅材合计价格
        let mtroPrice, metroPrice = 0, mtrolist = []
        if (item.weigh_data.materiel_info) {
          for (let i = 0, len = item.weigh_data.materiel_info; i < len.length; i++) {
            mtroPrice = len[i].pick_amount * len[i].unit_price
            mtrolist.push(mtroPrice)
          }
          for (let j = 0; j < mtrolist.length; j++) {
            metroPrice += mtrolist[j]
          }
        }

        //其他费用合计
        // otherPrice
        let othPrice, otherPrice = 0, otherlist = []
        if (item.weigh_data.other_price_info) {
          for (let i = 0, len = item.weigh_data.other_price_info; i < len.length; i++) {
            othPrice = len[i].price
            otherlist.push(othPrice)
          }
          for (let j = 0; j < otherlist.length; j++) {
            otherPrice += otherlist[j]
          }

        }
        //现买现卖其他费用
        let newOthPrice, newOtherPrice = 0,newOtherlist = []
        if (item.weigh_data.other_price_info ) {
          for (let i = 0, len = item.weigh_data.other_price_info; i < len.length; i++) {
            newOthPrice = len[i].price
            newOtherlist.push(newOthPrice)
          }
          for (let j = 0; j < newOtherlist.length; j++) {
            newOtherPrice += newOtherlist[j]
          }

        }


        that.setData({
          id: id,
          item: item,
          salseTotalPrice: salseTotalPrice,
          metroPrice: metroPrice,
          otherPrice: otherPrice,
          payInfoList: item.cargo_info,
          payInfoListClose: payInfoListClose,
          otherPriceList: otherPriceList,
          otherPriceListClose:[],
          newOtherPrice: newOtherPrice
        })
        console.log(otherPriceList)
      }

    })
    //获取系统配置，打款方式
    query({
      url: "api/ajax/sys_config",
      params: {},
      back: function (data) {
        console.log(data)
        let onesPay = data.result.pay_way
        let allList = [], pay_way
        let payinfo = []
        allList.push(onesPay)
        console.log(allList)
        for(let i in allList){
          const first = allList[i]
          for(let j in first ){
            payinfo.push(first[j])
            
             pay_way = first[j].value
          }
        }
        let payArray = that.data.payArray
        payArray.push(pay_way)
        
        that.setData({
          payArray: payArray,
          payinfo: payinfo
        })
      }
    })

  },
  bindPickerChange:function(e){
    const  that = this 
    const index = e.detail.value
    console.log(e)
    const payinfo = that.data.payinfo
    let pay_way_id
    for (let i = 0; i < payinfo.length;i++){
      if(i == index){
        pay_way_id = payinfo[i].key
      }  

      
    }
    this.setData({
      index: e.detail.value,
      pay_way_id: pay_way_id
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this
    const id = that.options.id
    that.setData({ id: id })
    const accountName = app.globalData.accountName
    const accountId = app.globalData.accountId

    const orderDetail = app.globalData.orderDetail
    const oldList = app.globalData.oldList
    console.log("accountId")
    console.log(accountId)
    let accontInfo = {}
    oldList && oldList.map(item=>{
      if (item.id == accountId){
        accontInfo = item
      }
    })
    that.setData({
      account: accountName,
      accountId: accountId,
      accontInfo: accontInfo
    })
  },
  //点击跳转选择收款账户
  onpickman:function(){
    const that = this 
    console.log(that.id)
    wx.navigateTo({
      url: '../pickAccount/pickAccount?id=' +  that.options.id,
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onOpen: function () {
    const that = this
    that.setData({
      open: true
    })
  },
  onSubmit: function () {

    const that = this
    const id = that.data.id
    const pay_way_id = that.data.pay_way_id

    if (!pay_way_id) {
      wx.showToast({
        title: '请填写完整',
        icon: 'none',
        duration: 1000
      })
      return;
    }

    const data = {
      id,
      pay_way_id
    }
    console.log(data)
    wx.showModal({
      title: '提示',
      content: '是否确认付款',
      success: function (res) {
        if (res.confirm) {
          create({
            url: 'api/waste/sell/pay',
            params: { ...data },
            back: function (data) {
              if (data.code == 200) {
                console.log(111)
                wx.navigateTo({ url: './paySuccess/paySuccess?id=' + id })
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    
  }
})