// pages/personal/purchase/recyclingOrder/routes/success/success.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  onBack: function () {
    const page = getCurrentPages()
    console.log("page")
    console.log(page)
    wx.navigateBack({
      delta: page.length - 2
    })
    // wx.redirectTo({ url: '/pages/personal/purchase/purchase' }) 
  },
  onView: function () {
    const that = this
    console.log(that.options)
    wx.navigateTo({ url: '../../successSalesView/successSalesView?id=' + that.options.id })
  }
})