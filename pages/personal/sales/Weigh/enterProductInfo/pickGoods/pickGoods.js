// pages/personal/purchase/orderDetail/pickGoods/pickGoods.js
import { create, query } from '../../../../../../utils/service.js'

const app = getApp()

Page({
  data: {
    tabs: ["塑料", "纸", "金属", "其他"],
    activeIndex: 0,
    selectedId: '',
    // PETItems: [
    //   { name: '透明不带胶PET', value: '0',id:'0' },
    //   { name: '钛白不带胶PET(厚)', value: '1', id: '1' },
    //   { name: '钛白不带胶PET(薄)', value: '2', id: '2'},
    //   { name: '蓝色不带胶PET', value: '3', id: '3'},
    //   { name: '透明带胶PET卷筒(厚)', value: '4', id: '4' },
    // ],
    // PPItems: [
    //   { name: '透明不带胶PET', value: '0' },
    //   { name: '钛白不带胶PET(厚)', value: '1' },
    //   { name: '钛白不带胶PET(薄)', value: '2' },
    //   { name: '蓝色不带胶PET', value: '3' },
    //   { name: '透明带胶PET卷筒(厚)', value: '4' },
    // ],
    firstList: '',
    secondList: '',
    thirdList: '',
    fourthList: '',
    status: '',
    list: []
  },
  onLoad: function (options) {
    console.log("pppppppppp")
    console.log(options)
    const status = options.status

    var that = this

    if (status == 1) {

      query({
        url: 'api/ajax/waste_cate_tree',
        params: {},
        back: function (data) {
          const list = data.result
          const tabs = []
          list && list.map(item => {
            tabs.push(item.name)
          })

          app.globalData.materialList = list

          const firstList = list[0]
          const secondList = list[1]
          const thirdList = list[2]
          const fourthList = list[3]

          firstList.child && firstList.child && firstList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          secondList.child && secondList.child && secondList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          thirdList.child && thirdList.child && thirdList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          fourthList.child && fourthList.child && fourthList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          that.setData({
            list: list,
            tabs: tabs,
            firstList: firstList,
            secondList: secondList,
            thirdList: thirdList,
            fourthList: fourthList,
            status: true
          })
        }
      })
    }


    if (status == 0) {
      query({
        url: 'api/ajax/waste_cate_tree',
        params: {},
        back: function (data) {
          const list = data.result
          console.log("list")
          console.log(list)
          list && list.map(item => {
            item.value = item.id
          })
          that.setData({
            list: list,
            status: false
          })
        }
      })
    }

    const firstList = app.globalData.firstList
    const secondList = app.globalData.secondList
    const thirdList = app.globalData.thirdList
    const fourthList = app.globalData.fourthList

    if (typeof (firstList) == 'string') {
      return;
    }
    that.setData({
      firstList: firstList,
      secondList: secondList,
      thirdList: thirdList,
      fourthList: fourthList
    })

  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  onSelect: function (e) {
    var that = this

    const activeIndex = that.data.activeIndex
    const status = that.data.status
    const firstList = that.data.firstList
    const secondList = that.data.secondList
    const thirdList = that.data.thirdList
    const fourthList = that.data.fourthList
    console.log(secondList)
    console.log(e)

    let parentName = e.currentTarget.dataset.parentname
    let firstName = e.currentTarget.dataset.firstname
    let price = e.currentTarget.dataset.price

    if (status == 1) {

      

      const parentMaterialType = parentName + "/" + firstName

      app.globalData.parentMaterialType = parentMaterialType
      app.globalData.standardQuotation = price

    }

    


    const id = e.currentTarget.dataset.idd
    const name = e.currentTarget.dataset.name
    const selectedId = that.data.selectedId

    app.globalData.enterGoodsName = name
    app.globalData.enterGoodsId = id
    console.log(id)
    // console.log("parentName")
    // console.log(parentName)

    that.setData({
      selectedId: id
    })

    // wx.navigateTo({
    //   url: '../enterProductInfo/enterProductInfo?name=' + name,
    // })
    wx.navigateBack({
      delta:1
    })

    // app.globalData.companyId = id
    // app.globalData.companyName = name

    // wx.navigateTo({ url: '/pages/personal/purchase/recyclingOrder/recyclingOrder' })
  }
});