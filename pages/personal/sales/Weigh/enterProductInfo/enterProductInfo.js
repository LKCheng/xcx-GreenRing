// pages/personal/purchase/orderDetail/enterProductInfo/enterProductInfo.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    id:'',
    status: '',
    showGoods:{},//货品信息列表
    standardQuotation: '',
    rouWeight:0,
    bucWeight:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const id = options.id
    that.setData({
      id:id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this 
    const list = app.globalData.Weighed_Goods_Information
    const goodsInfo = list.cargo_info
    let showGoods = {}
    const id = that.data.id
    console.log(id)
    goodsInfo && goodsInfo.map( item => {
      if(item.id == id){
        showGoods = item
      }
    })
    console.log(showGoods)
    that.setData({
      showGoods: showGoods
    })
  },  

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onrouWeight:function(e){
    const that = this 
    console.log(e)
    const rouWeight = e.detail.value
    that.setData({
      rouWeight: rouWeight
    })
  },
  onbucWeight: function (e) {
    const that = this
    console.log(e)
    const bucWeight = e.detail.value
    that.setData({
      bucWeight: bucWeight
    })
  },
  formSubmit: function (e) {
    const that = this
    console.log(e)
    const goodsNewInfo = e.detail.value
    const showGoods = that.data.showGoods
    const newGoodsInfo = app.globalData.newGoodsInfo
    if (goodsNewInfo.buckle_weight == '' || goodsNewInfo.net_weight == '' || goodsNewInfo.rough_weight =='' ){
      wx.showToast({
        title: '请完整填写',
        icon:'none',
        duration:1000
      })
    
    }else{
      showGoods.net_weight = Number(goodsNewInfo.net_weight)//净重
      showGoods.rough_weight = Number(goodsNewInfo.buckle_weight)//扣重
      showGoods.total_weight = Number(goodsNewInfo.rough_weight)//总重
      app.globalData.newGoodsInfo = showGoods
      wx.navigateBack({
        delta: 1,
      })
    }
    
  },
  onPick: function () {
    wx.navigateTo({
     url: "./pickGoods/pickGoods?status=" + this.data.status,
    })
  }
})