// pages/personal/purchase/orderDetail/extractMaterial/extractMaterial.js
import { create, query } from '../../../../../utils/service.js'

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    array:[],
    status: false,
    open: false,
   
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  const that = this 
    //获取系统配置，打款方式
    query({
      url: "api/ajax/sys_config",
      params: {},
      back: function (data) {
        console.log(data)
        let onesPay = data.result.materiel_type
        let typeList = []
        let array = that.data.array
        for( let x in onesPay){
          let matertype = onesPay[x]
          typeList.push(matertype)
        } 
        typeList && typeList.map( item => {
          array.push(item.value)
        })
        console.log(array)
        that.setData({
          array:array
        })
      }
    })

  },

  onOpen: function () {
    const that = this
    that.setData({
      open: true
    })
  },
  listenerPickerSelected: function (e) {
    //改变index值，通过setData()方法重绘界面
    const array = ["太空包", "分类箱", "编织袋", "人工叉车", "叉车称"]
    this.setData({
      index: e.detail.value,
      array:array
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  onblur:function(e){
    console.log(e)
  },
  onCodeBlur: function (e) {
    const that = this
    console.log(e)
    const codeValue = e.detail.value
    that.setData({
      codeValue: codeValue
    })
  },
  onAmountBlur: function (e) {
    const that = this
    console.log(e)
    const amountValue = e.detail.value
    that.setData({
      amountValue: amountValue
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    //获取选项
    query({
      url: 'api/ajax/sys_config',
      params: {},
      back: function (data) {
        const pickList = data.result
        app.globalData.pickList = pickList
      }
    })
    

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /*yxl*/
  formSubmit:function(e){
   
  },
  bindNumber:function(e){

    

  },
  bindCode:function(e){
    
  },
  onScan: function () {
    // 允许从相机和相册扫码
    wx.scanCode({
      success: (res) => {
        console.log(res)
        const status = res.errMsg
        if (status == 'scanCode:ok') {
          const result = res.result
          wx.navigateTo({
            url: '../materialInfo/materialInfo?result=' + result,
          })
        }
      },
      fail: (res) => {
        console.log(res)
      }
    })
  },
  formSubmit: function (e) {
    const that = this
    const metro = e.detail.value
    console.log(e)
    const array = that.data.array
    const index = that.data.index
    let value = ''
    for(let i=0;i<array.length;i++){
      if(i  == index){
        value = array[i]
      }
    }
    metro.type = value
    console.log(metro)
    let weighMateriel = app.globalData.weighMateriel
    weighMateriel.push(metro)
    if (metro.materiel_number == '' || metro.pick_amount == '' || metro.unit_price == '' ){
      wx.showToast({
        title: '请填写完整',
        icon:'none',
        duration:1000
      })
    }else{
      app.globalData.weighMateriel = weighMateriel
      wx.navigateBack({
        delta:1
      })
    }
  }
})