// pages/personal/sales/addSalesOrder/routes/pickerDocking/pickerDocking.js
import { create, query } from '../../../../../../utils/service.js'
const app = getApp()

Page({
  /**再来测试
   * 页面的初始数据
   */
  data: {
    workerList: [],
    pickDocking:[],
    nicknamelist:[],//对接人列表
    itemchecked:'',
    selectedId:'',
    selectedItem:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    const id = options.id
    var that = this
    const clientId = app.globalData.clientId
    console.log(clientId)
    const customer_id = id
    create({
      url: 'api/ajax/buyer_customer_list',
      params: { customer_id},
      back: function (data) {
        console.log(data)
        const result = data.result
        let obj = result
        let pickDocking  = []
        for(let key in obj){
          pickDocking.push(obj[key])
        }
        console.log(pickDocking)
        that.setData({
          pickDocking: pickDocking
        }) 
      }
    })

    const lastWorkerList = app.globalData.lastWorkerList
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      const that = this
      let greenSelect = false
     // let newnickname = {}
      let newnickname = app.globalData.dockingInfo
     // console.log(dockingInfo)
      let nicknamelist  =   that.data.nicknamelist
      if (newnickname){
        nicknamelist.push(newnickname)
        that.setData({
          nicknamelist: nicknamelist
        })
        console.log(nicknamelist) 
      }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  WorkCheckboxChange: function (e) {
    const that = this
    let  nicknamelist = this.data.nicknamelist
    let  values = e.detail.value;
    console.log(values)
    let selectvalue = ''
    for (let i = 0; i < nicknamelist.length; i++) {
      console.log(nicknamelist[i].value)
      selectvalue = nicknamelist[i].value
    }
    if(values == selectvalue){
      // console.log(11)
      // itemchecked : 
    }
   
  },
  bindViewTap: function (e) {
    console.log(e)
    const that = this

    const id = e.currentTarget.dataset.ddd
    const pickDocking = that.data.pickDocking
    const selectedId = that.data.selectedId

    let selectedItem
    pickDocking && pickDocking.map(item => {
      if (item.id == id) {
        selectedItem = item
        that.setData({
          selectedId: '',
          selectedItem: item
        })
        return;
      }
      that.setData({
        selectedId: id
      })
    })
    app.globalData.dockingInfo = selectedItem
    wx.navigateBack({
      delta: 1
    })
  },
  onAdd: function (e) {
    wx.navigateBack({
      delta:1
    })
    
  },
  onSave: function () {
    var that = this
    const itemchecked = that.data.itemchecked
    const selectedItem = that.data.selectedItem
    console.log(selectedItem)
    
  

    if (selectedItem == ''){
      wx.showToast({
        title: '请选择',
        icon:'none',
        duration:1000
      })
    }else{
      app.globalData.dockingInfo = selectedItem
      wx.navigateBack({
        delta: 1
      })
    }
 
  }
})