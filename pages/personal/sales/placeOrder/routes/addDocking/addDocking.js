// pages/personal/sales/addSalesOrder/routes/addDocking/addDocking.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    clientName: '',
    clientFactory: '',
    name: '',
    phone: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    const clientName = app.globalData.clientName
    const clientFactory = app.globalData.clientFactory
    that.setData({
      clientName: clientName,
      clientFactory: clientFactory
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSave: function () {

    const that = this
    const dockingInfo = {}

    dockingInfo.name = that.data.name
    dockingInfo.value = that.data.phone
    
    //console.log(dockingInfo.phone)
    if (dockingInfo.name === '' || dockingInfo.phone === ''){
      wx.showToast({
        title: '请填写完整',
        icon:'none',
        duration:1000
      })
    }else{
      app.globalData.dockingInfo = dockingInfo

      wx.navigateBack({
        delta: 1
      })

    }
    
  },
  onBlur:function (e) {
  const inputType = e.currentTarget.dataset.type
  const value = e.detail.value
  const that = this
  if (inputType == 'name'){
    that.setData({
      name: value
    })
  }
  if (inputType == 'phone') {
    that.setData({
      phone: value
    })
  }
  }
})