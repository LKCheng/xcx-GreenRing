// pages/personal/sales/addSalesOrder/routes/pickFirstLevel/pickFirstLevel.js
import { create, query } from '../../../../../../utils/service.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],//一级分类
    selectedFristName:"",//被选中的id，
    selectedId: "",
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that =  this 
    
  },
  onSelect:function(e){
    const that = this
    console.log(e)
    const selectedId = e.currentTarget.dataset.itemid
    const selectedFristName = e.currentTarget.dataset.name
    const id = that.data.selectedId
    if (id == selectedId) {
      that.setData({
        selectedId: ''
      })
      return;
    }

    that.setData({
      selectedId: selectedId
    })
   
    app.globalData.selectedFristName = selectedFristName
    
    wx.navigateBack({
      delta: 1
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this 
    const list = app.globalData.firstClassificationList
    
    that.setData({
      list: list
    })
    const selectedId = that.selectedId
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})