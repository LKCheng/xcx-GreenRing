// pages/personal/sales/addSalesOrder/addSalesOrder.js
import { create, query } from '../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    clientId: '', 
    clientName: '',
    clientFactory: '',
    saleAddressId: '',
    saleAddressName: '',
    dockingInfo:{},
    dockingId:'',//对接人id
    remark:'',//留言
    time:'',
    date: '',
    startTime: '',
    startX: 0, //开始坐标
    startY: 0,
    items:[],
    sortList:[],
    className:'',
    total:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    for (var i = 0; i < 2; i++) {
      this.data.items.push({
        content: i + " 向左滑动删除哦,向左滑动删除哦,向左滑动删除哦,向左滑动删除哦,向左滑动删除哦",
        isTouchMove: false //默认全隐藏删除
      })
    }
    this.setData({
      items: this.data.items
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this
    const clientId = app.globalData.clientId
    console.log(clientId)
    const clientName = app.globalData.clientName//客户名称 
    const clientFactory = app.globalData.clientFactory //客户公司名称
    const saleAddressId = app.globalData.saleAddressId
    const saleAddressName = app.globalData.saleAddressName
    const dockingInfo = app.globalData.dockingInfo
    const dockingId = dockingInfo.id
    const showname =  app.globalData.showname
    const customerInfo =  app.globalData.customerInfo
    const sortList = app.globalData.sortList
    const prices = []

    const firstName =  app.globalData.firstListName
    
    let showNameList = []
    for(let i=0;i<sortList.length;i++){
      sortList[i].sortWeight
      const price = sortList[i].pricevalue * sortList[i].sortWeight
      
      prices.push(price)
    }
    let total = 0
    for(let i=0;i<prices.length;i++){
      total += Number(prices[i])
      
    }
    
    //获取当前时间，格式YYYY-MM-DD
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
      month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
      strDate = "0" + strDate;
    }
    var startTime = year + seperator1 + month + seperator1 + strDate;


    that.setData({
      clientId: clientId,
      clientName: clientName,
      customerInfo:customerInfo,
      clientFactory: clientFactory,
      saleAddressId: saleAddressId,
      saleAddressName: saleAddressName,
      dockingInfo: dockingInfo,
      dockingId :dockingId ,
      startTime: startTime,
      sortList: sortList,
      total:total
    })


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  onpickdoking:function(){
    const id = this.data.clientId
    if(id == ''){
      wx.showToast({
        title: '请先选择客户',
        icon:'none',
        duration:1000
      })
    }else{
      wx.navigateTo({
        url: './routes/pickDocking/pickDocking',
      })
    }
    
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  }, 
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },
  //手指触摸动作开始 记录起点X坐标
  touchstart: function (e) {
    //开始触摸时 重置所有删除
    this.data.sortList.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })
    this.setData({
      startX: e.changedTouches[0].clientX,
      startY: e.changedTouches[0].clientY,
      sortList: this.data.sortList
    })
  },
  //滑动事件处理
  touchmove: function (e) {
    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      startX = that.data.startX,//开始X坐标
      startY = that.data.startY,//开始Y坐标
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.sortList.forEach(function (v, i) {
      v.isTouchMove = false

      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true
      }
      
    })
    //更新数据
    that.setData({
      sortList: that.data.sortList
    })
  },
  /**
   * 计算滑动角度
   * @param {Object} start 起点坐标
   * @param {Object} end 终点坐标
   */
  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  //删除事件
  del: function (e) {
    console.log(e)
    this.data.sortList.splice(e.currentTarget.dataset.index, 1)
    // let total = this.data.total
    // console.log(this.data.sortList)
    let  sortList = this.data.sortList
    let prices = []
    for (let i = 0; i < sortList.length; i++) {
      const price = sortList[i].pricevalue * sortList[i].sortWeight
      prices.push(price)
    }
    let total = 0
    for (let i = 0; i < prices.length; i++) {
      total += Number(prices[i])
    }
    app.globalData.sortList = sortList
    this.setData({
      sortList: this.data.sortList,
      total: total
    })
  },
  onAdd: function () {
    wx.navigateTo({
      url: './routes/pickGoods/pickGoods',
    })
  },
  onBlur:function(e){
    const that = this 
    const remark = e.detail.value
    that.setData({
      remark: remark
    })
  },
  //提交
  onSubmit:function(){
    const that = this 
    const { clientId, dockingId, remark, saleAddressId, date, time, sortList} = that.data
          //客户id ， 对接人id， 留言，   出货地id，     年月，时分
    let cargo = {}
    let cargo_info = []
    for (let i=0;i< sortList.length;i++){
      cargo = {
        cate_id: sortList[i].cate_id,
        plan_sell_weight: sortList[i].sortWeight,
        unit_price: sortList[i].pricevalue
    }
      cargo_info.push(cargo)
    }
    
    const cargo_pick_time = date +' ' + time 
    const data = {
      customer_id: clientId,
      customer_linkman_id: dockingId,
      seller_remark: remark,
      warehouse_id: saleAddressId,
      cargo_pick_time: cargo_pick_time,
      cargo_info: cargo_info,
      is_draft:0
    }
    console.log(data)
    if (clientId == '' || saleAddressId == '' || date == '' || time == '' ){
      wx.showToast({
        title: '请填写完整',
        icon:'none',
        duration:1000
      })
    } else if (!dockingId){ 
      wx.showToast({
        title: '请填写完整',
        icon: 'none',
        duration: 1000
      })
    } else if (sortList == ''){
      wx.showToast({
        title: '请添加货品',
        icon: 'none',
        duration: 1000
      })
    }else{
      wx.showModal({
        title: '提示',
        content: '是否确认提交',
        success:function(res){
          if (res.confirm){
            create({
              url: 'api/waste/sell/add',
              params: { ...data },
              back: function (data) {
                if (data.code == 200) {
                  wx.navigateTo({
                    url: '../createSuccess/createSuccess',
                  })
                }
              }
            })
            
          }
        }
      })
    }

    
  } 
})