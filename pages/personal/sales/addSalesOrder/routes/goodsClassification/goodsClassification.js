import { create, query } from '../../../../../../utils/service.js'

const app = getApp()

Page({
  data: {
    tabs: [],
    activeIndex: '',
    // PETItems: [
    //   { name: '透明不带胶PET', value: '0' },
    //   { name: '钛白不带胶PET(厚)', value: '1' },
    //   { name: '钛白不带胶PET(薄)', value: '2' },
    //   { name: '蓝色不带胶PET', value: '3' },
    //   { name: '透明带胶PET卷筒(厚)', value: '4' },
    // ],
    // PPItems: [
    //   { name: '透明不带胶PET', value: '0' },
    //   { name: '钛白不带胶PET(厚)', value: '1' },
    //   { name: '钛白不带胶PET(薄)', value: '2' },
    //   { name: '蓝色不带胶PET', value: '3' },
    //   { name: '透明带胶PET卷筒(厚)', value: '4' },
    // ],
    firstList: '',
    secondList: '',
    thirdList: '',
    fourthList: '',
    status: '',
    list: [],
    showname:''
  },
  onLoad: function (options) {

    console.log(options)
    const id = options.id
    console.log("id")
    console.log(id)
    var that = this

    //if (id == 1) {
    Array.prototype.del = function (n) {　//n表示第几项，从0开始算起。
      //prototype为对象原型，注意这里为对象增加自定义方法的方法。
      if (n < 0)　//如果n<0，则不进行任何操作。
        return this;
      else
        return this.slice(0, n).concat(this.slice(n + 1, this.length));
      /*
       concat方法：返回一个新数组，这个新数组是由两个或更多数组组合而成的。
       这里就是返回this.slice(0,n)/this.slice(n+1,this.length)
       组成的新数组，这中间，刚好少了第n项。
       slice方法： 返回一个数组的一段，两个参数，分别指定开始和结束的位置。
      */
    }
      create({
        url: 'api/ajax/waste_cate_tree',
        params: {},
        back: function (data) {
          let list = data.result
          
          
         
          // list = list.del(0);　//从0算起，删除第4项。
          const tabs = []
          console.log(data)
          list && list.map(item => {
            tabs.push(item.name)
          })
          const firstList = list[0]
          const secondList = list[1]
          const thirdList = list[2]
          const fourthList = list[3]

          firstList.child && firstList.child && firstList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          secondList.child && secondList.child && secondList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          thirdList.child && thirdList.child && thirdList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })
          fourthList.child && fourthList.child && fourthList.child.map(item => {
            item.child && item.child.map(childItem => {
              childItem.value = childItem.id
            })
          })

          const lastFirstList = app.globalData.firstList
          const lastSecondList = app.globalData.secondList
          const lastThirdList = app.globalData.thirdList
          const lastFourthList = app.globalData.fourthList

          console.log("lastSecondList")
          console.log(typeof (lastSecondList))
          console.log(lastSecondList)


          that.setData({
            list: list,
            tabs: tabs,
            firstList: typeof (lastFirstList) == 'object' ? lastFirstList : firstList,
            secondList: typeof (lastSecondList) == 'object' ? lastSecondList : secondList,
            thirdList: typeof (lastThirdList) == 'object' ? lastThirdList : thirdList,
            fourthList: typeof (lastFourthList) == 'object' ? lastFourthList : fourthList,
            status: true
          })
        }
      })
    //}

    // if (id == 2) {
    //   query({
    //     url: 'api/ajax/waste_cate_tree',
    //     params: {},
    //     back: function (data) {
    //       const list = data.result
    //       console.log(list)
    //       list && list.map(item => {
    //         item.value = item.id
    //       })

    //       const uncategorizedList = app.globalData.uncategorizedList

    //       that.setData({
    //         list: uncategorizedList.length > 0 ? uncategorizedList : list,
    //         status: false
    //       })
    //     }
    //   })
    // }
  },
  onShow:function(){
    app.globalData.className = '' 

  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  // onChange:function(e){
  //   console.log(e)

  // },
  onclicked:function(e){
    const that = this
    console.log(e.currentTarget.dataset.name)
    const firstList = that.data.firstList
    const secondList = that.data.secondList
    const thirdList = that.data.thirdList
    const fourthList = that.data.fourthList
    console.log(e)
    const parent_id = e.currentTarget.dataset.parent_id
    let item = {}
    

    for (let i = 0, list = firstList.child; i <list.length;i++ ){
      item = list[i]
      const className = list[i].name
      app.globalData.firstListName = className
      
    }
    for (let i = 0, list = secondList.child; i < list.length; i++) {
      item = list[i]
      const className = list[i].name
      app.globalData.secondListName = className
    }
    for (let i = 0, list = thirdList.child; i < list.length; i++) {
      item = list[i]
      const className = list[i].name
      app.globalData.thirdListName = className
    }
    for (let i = 0, list = fourthList.child; i < list.length; i++) {
      item = list[i]
      const className = list[i].name
      app.globalData.fourthListName = className
    }
    const showname = e.currentTarget.dataset.name
    const cate_id = e.currentTarget.dataset.id
    const storage_weight = e.currentTarget.dataset.weight
    that.setData({
      showname: showname
    }) 
    app.globalData.showname = showname
    app.globalData.cate_id = cate_id
    app.globalData.storage_weight = storage_weight
    app.globalData.parent_id = parent_id
    wx.navigateBack({
      delta: 1
    })
  },
  // firstChange: function (e) {
  //   var firstList = this.data.firstList, values = e.detail.value;
  //   firstList.child && firstList.child && firstList.child.map(item => {
  //     item.child && item.child.map(childItem => {
  //       childItem.checked = false
  //       for (var j = 0, lenJ = values.length; j < lenJ; ++j) {
  //         if (childItem.value == values[j]) {
  //           childItem.checked = true;
  //           break;
  //         }
  //       }
  //     })
  //   })
  //   this.setData({
  //     firstList: firstList
  //   });
  // },
  // secondChange: function (e) {
  //   var secondList = this.data.secondList, values = e.detail.value;
  //   secondList.child && secondList.child && secondList.child.map(item => {
  //     item.child && item.child.map(childItem => {
  //       childItem.checked = false
  //       for (var j = 0, lenJ = values.length; j < lenJ; ++j) {
  //         if (childItem.value == values[j]) {
  //           childItem.checked = true;
  //           break;
  //         }
  //       }
  //     })
  //   })
  //   console.log(e)
  //   console.log(values)
  //   this.setData({
  //     secondList: secondList
  //   });
  // },
  // thirdChange: function (e) {
  //   var thirdList = this.data.thirdList, values = e.detail.value;
  //   thirdList.child && thirdList.child && thirdList.child.map(item => {
  //     item.child && item.child.map(childItem => {
  //       childItem.checked = false
  //       for (var j = 0, lenJ = values.length; j < lenJ; ++j) {
  //         if (childItem.value == values[j]) {
  //           childItem.checked = true;
  //           break;
  //         }
  //       }
  //     })
  //   })
  //   this.setData({
  //     thirdList: thirdList
  //   });
  // },
  // fourthChange: function (e) {
  //   var fourthList = this.data.fourthList, values = e.detail.value;
  //   fourthList.child && fourthList.child && fourthList.child.map(item => {
  //     item.child && item.child.map(childItem => {
  //       childItem.checked = false
  //       for (var j = 0, lenJ = values.length; j < lenJ; ++j) {
  //         if (childItem.value == values[j]) {
  //           childItem.checked = true;
  //           break;
  //         }
  //       }
  //     })
  //   })
  //   this.setData({
  //     fourthList: fourthList
  //   });
  // },
  WorkCheckboxChange: function (e) {
    var list = this.data.list, values = e.detail.value;
    console.log(values)
    console.log(list)
    for (var i = 0, lenI = list.length; i < lenI; ++i) {
      list[i].checked = false;

      for (var j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (list[i].value == values[j]) {
          list[i].checked = true;
          break;
        }
      }
    }

    this.setData({
      list: list
    });
  },
  onOk: function (e) {
    var that = this
    const firstList = that.data.firstList
    const secondList = that.data.secondList
    const thirdList = that.data.thirdList
    const fourthList = that.data.fourthList
    const uncategorizedList = that.data.list

    app.globalData.firstList = firstList
    app.globalData.secondList = secondList
    app.globalData.thirdList = thirdList
    app.globalData.fourthList = fourthList

    console.log("secondList")
    console.log(secondList)

    app.globalData.uncategorizedList = uncategorizedList


    // wx.navigateTo({ url: '/pages/personal/purchase/recyclingOrder/recyclingOrder' }) 

    // wx.navigateBack({
    //   delta: 1
    // })

  },
  onLook: function () {

  }
});