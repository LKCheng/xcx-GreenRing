// pages/personal/sales/addSalesOrder/routes/pickClient/pickClient.js
import { create, query } from '../../../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    selectedValue: '',
    incharge:'',//负责人
    name:'',//公司名称
    mobile:'',//联系电话
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    query({
      url:'api/ajax/customer_list?type=down',
      params:{},
      back:function(data){
        const list = data.result
        let newList 
        console.log(list)
        // for(let x in list ){
        //   console.log(list[x])
        //   console.log(x)
        // }
       
        that.setData({
          list:list
        })
      }
    })


  

    // const clientId = app.globalData.clientId

    // that.setData({
    //   //list: list,
    //   selectedId: clientId
    // })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSelect: function (e) {//选择地址
    var that = this
    const data = that.data
    const list = that.data.list
    let customerInfo
    console.log(list)
    const value  = e.currentTarget.dataset.value.name
    for(let item in list ){
     
      if(value == list[item].name){
        customerInfo = list[item].name  
        app.globalData.clientId = list[item].id
      }
    }
    const selectedValue = that.data.selectedValue
    
    if (value == selectedValue) {
      that.setData({
        selectedValue: ''
      })
      return;
    }
    that.setData({
      selectedValue : value
    })

      
    app.globalData.customerInfo = customerInfo
    console.log(e)
    wx.navigateBack({
      delta: 1
    })

  }
})