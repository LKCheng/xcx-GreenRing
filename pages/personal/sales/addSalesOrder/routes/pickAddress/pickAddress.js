// pages/personal/sales/addSalesOrder/routes/pickAddress/pickAddress.js
import { create, query } from '../../../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],//仓库列表
    selectedId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    const saleAddressId = app.globalData.saleAddressId
    that.setData({
      selectedId: saleAddressId
    })
    create({
      url: 'api/ajax/warehouse_tree',
      params: {},
      back: function (data) {
        console.log(data)
        const list = data.result
        that.setData({
          list :list 
        })
      }
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onSelect: function (e) {//选择公司
    var that = this
    const data = that.data
    const id = e.currentTarget.dataset.companyid
    const name = e.currentTarget.dataset.companyname
    const selectedId = that.data.selectedId
    if (id == selectedId) {
      that.setData({
        selectedId: ''
      })
      return;
    }
    that.setData({
      selectedId: id
    })

    app.globalData.saleAddressId = id
    app.globalData.saleAddressName = name

    // wx.navigateTo({ url: '/pages/personal/purchase/recyclingOrder/recyclingOrder'}) 

    wx.navigateBack({
      delta: 1
    })
  }
})