// pages/personal/sales/addSalesOrder/routes/pickGoods/pickGoods.js
import { create, query } from '../../../../../../utils/service.js'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    pGoodName: '',
    pGoodId: '',
    sortGoodId: '',
    sortGoodName: '',
    //value: '',//预售重量
    showname:'',
    pricevalue:0,//出售单价
    //storage_weight:'',//库存重量
    parent_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    //const list = app.globalData.firstList
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this
    const showname = app.globalData.showname
    const cate_id  =   app.globalData.cate_id
    const storage_weight = app.globalData.storage_weight
    const parent_id = app.globalData.parent_id
    that.setData({
      
      showname: showname,
      cate_id: cate_id,//货品id
      storage_weight: storage_weight,//库存重量
      parent_id: parent_id
    })
    //获取货品分类
    create({
      url: 'api/ajax/waste_cate_tree',
      params: {},
      back: function (data) {
        console.log(data)
        const list = data.result
        app.globalData.firstClassificationList = list
        console.log(list)

      }
    })
    //
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onInput: function (e) {
    const that = this
    const value = e.detail.value
    that.setData({
      value: value
    })

  },
  onpriceInput:function(e){
    const that = this
    const value = e.detail.value
    that.setData({
      pricevalue: value
    })

  },
  onPick: function (e) {
    const pickType = e.currentTarget.dataset.type
    if (pickType == 'get') {
      wx.navigateTo({
        url: '../pickPurchaseGoods/pickPurchaseGoods',
      })
    }
    if (pickType == 'sort') {
      wx.navigateTo({
        url: '../pickPurchaseSort/pickPurchaseSort',
      })
    }
  },
  onSubmit: function () {
    const that = this.data
    const list = app.globalData.sortList
    const value = that.value
    const sortWeight = Number(that.value)//重量
    const cate_id = that.cate_id//货品id
    const showname = that.showname//货品名
    const pricevalue = Number(that.pricevalue)//单价
    const parent_id = that.parent_id
    
    const sortItem = {
      cate_id: cate_id,//id
      sortWeight: sortWeight,
      showname:showname,
      pricevalue: pricevalue,

     // parent_id: parent_id
    }
   
     function  isNumber(obj){
       return obj ===  +obj
     }
     if (isNumber(pricevalue) === false || isNumber(sortWeight) === false){
       wx.showToast({
         title: '请正确填写',
         icon: 'none',
         duration: 1000
       })
       return
     }

    if(!cate_id){
      wx.showToast({
        title: '请选择货品分类',
        icon:'none',
        duration:1000
      })
    } else if (pricevalue == ''){
      wx.showToast({
        title: '请填写出售单价',
        icon: 'none',
        duration: 1000
      })
    } else if (sortWeight == '') {
      wx.showToast({
        title: '请填预售重量',
        icon: 'none',
        duration: 1000
      })
    }else{
      list.push(sortItem)

      app.globalData.sortList = list

      // 清空缓存
      app.globalData.cate_id = ''
      app.globalData.storage_weight = ''
      app.globalData.showname = ''
      
      wx.navigateBack({
        delta: 1
      })

    }
    

  }
})